import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import axios from 'axios'

class AddPost extends Component{
  constructor(props){
    super(props)
    this.state={
                login:this.props.login,
                userkey:this.props.childkey,
                postId:this.props.postId,
                text:this.props.text,
                time:this.props.time,
                imgurl:this.props.imgurl,
                nblikes:this.props.nblikes,
                liked:false
              }
  }


    handleChange = evt => {
      this.setState({ text: evt.target.value });
    }

    enregistrer(){
      if(this.state.text=="  "){
          alert("vous pouvez pas inserer un post vide");
      }else{
    /*  this.props.addPost(this.state)
      this.setState({ text: "" })*/
      axios.get('http://localhost:8080/hichem_kiki_3i017/AddPost?login='+this.state.login+'&key='+this.state.userkey+'&text='+this.state.text).then(
        resp=>{
          if(resp.data.status=="ko"){
            alert(resp.data.message)
            this.props.reinit();
            this.setState({text:""});
          }else if(resp.data.status=="ok"){
            alert("Post Ajouté!")
            this.props.reinit();
            this.setState({text:""});
          }
        });
      }
    }

  render(){
    return(
      <div className="AddPost" class="postcontainer">
        {this.props.login} {/*this.props.childkey*/}
        <div class="columnimg">
        <img src={require("./images/user.png")}/>
        {/*<img src={require("state.imgurl")}/>*/}
        </div>
        <div class="columninfo">
          <textarea rows="3" cols="75"
                    name="comment" form="usrform"
                    value={this.state.text}
                    onChange={this.handleChange}
                    class="com" placeholder="Enter text here...">
          </textarea>
          <input type="button" onClick={()=>this.enregistrer()}  value="Enregistrer"
                                          class="btn btn-info align-self-end mx-2"
                                          disabled={!this.state.text}/>
        </div>
      </div>
    )
  }
}
export default AddPost;
