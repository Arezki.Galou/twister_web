import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import axios from 'axios';


class Signin extends Component{
	constructor(props){
		super(props);
		this.state={nom:"",
								prenom:"",
								login:"",
								sexe:"",
								age:"",
								password:"",
								spassword:"",
							 }
	}

	handleNomChange = evt => {
    this.setState({ nom: evt.target.value });
  };

	handlePrenomChange = evt => {
    this.setState({ prenom: evt.target.value });
  };

	handleSexeChangeToMale = evt => {
		this.setState({ sexe: "Homme" });
	};
	handleSexeChangeToFemale = evt => {
		this.setState({ sexe: "Femme" });
	};

	handleAgeChange = evt => {
	   this.setState({ age: evt.target.value });
	};

	handleLoginChange = evt => {
    this.setState({ login: evt.target.value });
  };

  handlePasswordChange = evt => {
    this.setState({ password: evt.target.value });
  };

	handleSPasswordChange = evt => {
    this.setState({ spassword: evt.target.value });
  };

	canBeSubmitted() {
	  const { nom, prenom, age, sexe, login, password, spassword } = this.state;
	  return (
			nom.length > 0 &&
			prenom.length > 0 &&
			age > 0 &&
			sexe.length > 0 &&
	    login.length > 0 &&
	    password.length > 0 &&
			spassword.length > 0 &&
			spassword===password
	  );
	}

	handleSubmit = (evt) => {
	  if (!this.canBeSubmitted()) {
	    evt.preventDefault();
	    return;
		}
		axios.get('http://localhost:8080/hichem_kiki_3i017/CreateUser?login='+this.state.login
		+'&password='+this.state.password+'&nom='+this.state.nom+'&prenom='+this.state.prenom+'&age='+this.state.age+'&sexe='+this.state.sexe).then(
		resp=>{
						if(resp.data.status==="ok"){
							alert("INSCRIPTION REUSSI");
							this.props.setPageToLogin();
						}else{
							alert(resp.data.message);
						}});

	};

	render(){
		const isEnabled = this.canBeSubmitted();
		return(
    	<div id="connexion_main" class="form">
				<h1> Inscription </h1>
				<form action="javascript:(function(){return;})()" method="get" onSubmit="javascript:connexion(this)"/>
				<div class="ids">
					<span>Nom</span>
					<input  type="text"
									value={this.state.nom}
									onChange={this.handleNomChange}
									name="nom" />
				</div>
				<div class="ids">
					<span>Prenom</span>
					<input 	type="text"
									value={this.state.prenom}
									onChange={this.handlePrenomChange}
									name="prenom" />
				</div>
				<div class="ids">
					<span>Login</span>
					<input  type="text"
									value={this.state.login}
									onChange={this.handleLoginChange}
									name="login" />
				</div>
				<div class="ids">
					<span>Email</span>
					<input type="text" name="pass"/>
				</div>


				<div class="ids">
				<form><pre><input type="radio"
													name="gender"
													value={this.state.sexe}
													onChange={this.handleSexeChangeToMale}
													Checked/> Male <input 	type="radio"
																			name="gender"
																			value={this.state.sexe}
																			onChange={this.handleSexeChangeToFemale}/> Female	</pre></form>

				</div>
				<div class="ids">
					<span>Age</span>
					<input 	type="number"
									name="age"
									value={this.state.age}
									onChange={this.handleAgeChange}
									min="0"
									max="150"/>
				</div>


				<div class="ids">
					<span>Mot de Passe</span>
					<input 	type="password"
									value={this.state.password}
									onChange={this.handlePasswordChange}/>
				</div>
				<div class="ids">
					<span>Retapez le mot de passe</span>
					<input 	type="password"
									value={this.state.spassword}
									onChange={this.handleSPasswordChange}/>
				</div>

				<div class="buttons">
					<input 	type="submit"
									value="Enregistrer"
									disabled={!isEnabled}
									onClick={this.handleSubmit} />
				</div>
				<div class="links">
						<input	type="submit"
										value="Annuler"
										onClick={()=>this.props.setPageToLogin()} />
				</div>
    	</div>)
    }
}
export default Signin;
