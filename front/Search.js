import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import ProfileThumbnail from "./ProfileThumbnail";
import Comment from "./Comment";
import AddPost from "./AddPost";
import AddComment from "./AddComment";
import Post from "./Post";
import axios from 'axios';

class Search extends Component{

	constructor(props){
		super(props);
		this.state={
            login:this.props.getLogin(),
			userkey:this.props.getKey(),
            result:[],
            searchtype:"1",
            query:"",
            nbres:0};
        this.addElement=this.addElement.bind(this);
        this.search=this.search.bind(this);
        this.cancel=this.cancel.bind(this);
        this.setProfile2=this.setProfile2.bind(this);
        this.callThis.bind(this);

    }
    callThis = (e) => {
        this.setState({searchtype: e.target.value, result:[], query:"",nbres:0});

    }
    setProfile2(data){
        this.props.setProfile1(data);
    }

    search(){

        this.cancel()
        if(this.state.searchtype==="1" && this.state.query!=" "){
            axios.get('http://localhost:8080/hichem_kiki_3i017/SearchUsers?login='
                +this.state.login+'&key='
                +this.state.userkey+'&query='
                +this.state.query).then(
                    resp=>{
                        if(resp.data.status=="ok"){

                            resp.data.Users.map( (c)=> {
                                this.addElement(c);
                            });
                        }else{
                            alert(resp.data.message);
                        }
                    }
                )
        }else if(this.state.searchtype==="2" && this.state.query!=" "){

          alert("chargement des posts");
          axios.get('http://localhost:8080/hichem_kiki_3i017/Search?login='
          +this.state.login+'&key='
          +this.state.userkey+'&query='
          +this.state.query).then(
              resp=>{
                  if(resp.data.status=="ok"){
                      resp.data.postList.map( (c)=> {
                          this.addElement(c);
                      });
                  }else{
                      alert(resp.data.message);
                  }
              }
          )
        }
    }


    addElement(element){
        this.state.result.push(element);
        this.setState({nbres:this.state.nbres+1})
    }

    cancel(){
        this.setState({result:[]});
    }

    handleQueryChange = evt => {
        this.setState({ query: evt.target.value });
    };

	render(){

   if (this.state.searchtype==="1") {


    return(
        <div className="Home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2" >
                    </div>
                    <div class="col-lg-8 page" >
                        <select     onChange={this.callThis}
                            name="cars"
                            class="searchchoice">

                            <option value = "1">Utilisateurs</option>
                            <option value = "2">Contenu</option>
                        </select>
                        <input  type="text"
                                placeholder="search text here"
                                value={this.state.query}
                                onChange={this.handleQueryChange}
                                class="searchbox" />
                        <input  type="submit"
                                value=" Search "
                                class="searchsubmit"
                                onClick={this.search}/>
                        <div class="searchres">

                                    {this.state.result.map( (c) => { if(c!=""){
                                                                console.log(c.login);
                                                                return <ProfileThumbnail
                                                                            setProfilef={this.setProfile2}
                                                                            userlogin={this.state.login}
                                                                            userkey={this.state.userkey}
                                                                            profilelogin={c.login}
                                                                            nom={c.nom}
                                                                            prenom={c.prenom}
                                                                            type={c.type}
                                                                            sexe={c.sexe}
                                                                            age={c.age}
                                                                            imgurl={"some url"}/>;
                                                              }}
                                                     )
                                }

                        </div>
                    </div>
                    <div class="col-lg-2" >
                    </div>
                </div>
            </div>
        </div>
    )

   } else {

    return(
        <div className="Home">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2" >
                    </div>
                    <div class="col-lg-8 page" >
                        <select   onChange={(event)=>this.callThis(event)}
                            name="cars"
                            class="searchchoice">

                            <option value = "1">Utilisateurs</option>
                            <option value = "2">Contenu</option>
                        </select>
                        <input  type="text"
                                placeholder="search text here"
                                value={this.state.query}
                                onChange={this.handleQueryChange}
                                class="searchbox" />
                        <input  type="submit"
                                value=" Search "
                                class="searchsubmit"
                                onClick={this.search}/>
                        <div class="searchres">

                                    {this.state.result.map( (c) => { if(c!=""){

                                                                return 	<Post
			                                                                childkey={this.state.userkey}
			                                                                userlogin={this.state.login}
			                                                                authorlogin={c.login}
			                                                                postId={c.post_id}
			                                                                text={c.text}
			                                                                time={c.date}
			                                                                imgurl={"some url"}
			                                                                comList={c.comList}
			                                                                likeList={c.likeList}
			                                                                nblikes={0}
			                                                                liked={false}
			                                                                reinit={this.reinit}/>;
                                                              }}
                                                     )
                                }

                        </div>
                    </div>
                    <div class="col-lg-2" >
                    </div>
                </div>
            </div>
        </div>
    )


   }

	}
}
export default Search;
