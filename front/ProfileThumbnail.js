import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import axios from 'axios';


class ProfileThumbnail extends Component{
    constructor(props){
        super(props);
        this.state={
            userlogin:this.props.userlogin,//Le login de l'utilisateur
            userkey:this.props.userkey,//La clé de l'utilisateur
            login:this.props.profilelogin,//Le login tu propriétaire du compte qu'on visite
            nom:this.props.nom,
            prenom:this.props.prenom,
            age:this.props.age,
            sexe:this.props.sexe,
            imgurl:this.props.imgurl,
            type:this.props.type,
            buttonvalue:"none",
            button2:true,
            button2value:"none"
        }
        this.consulterProfile=this.consulterProfile.bind(this);

    }

    consulterProfile(){
        const data=this.state.login;
        this.props.setProfilef(data);
    }

    componentDidMount(){
        if(this.state.type==="fr"){
            this.setState({buttonvalue:"Remove Follower",button2:true,button2value:"Follow"})
        }else if(this.state.type==="fd"){
            this.setState({buttonvalue:"Unfollow",button2:false})
        }else if(this.state.type==="o"){
            this.setState({buttonvalue:"Follow",button2:false})
        }else if(this.state.type==="frfd"){
            this.setState({buttonvalue:"Remove Follower",button2:true,button2value:"Unfollow"})
        }
    }

    handleClick1 = evt=>{
        if(this.state.type==="fr"){
            axios.get('http://localhost:8080/hichem_kiki_3i017/RemoveFollower?login='
            +this.state.userlogin+'&key='
            +this.state.userkey+"&login_follower="
            +this.state.login).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.setState({type:"o"});
                        this.setState({buttonvalue:"Follow",button2:false});
                    }else{
                        alert(resp.data.message);
                    }})
        }else if(this.state.type==="fd"){
            axios.get('http://localhost:8080/hichem_kiki_3i017/Unfollow?login='
            +this.state.userlogin+'&key='
            +this.state.userkey+"&login_followed="
            +this.state.login).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.setState({type:"o"});
                        this.setState({buttonvalue:"Follow",button2:false});
                    }else{
                        alert(resp.data.message);
                    }})
        }else if(this.state.type==="o"){
            axios.get('http://localhost:8080/hichem_kiki_3i017/Follow?login='
            +this.state.userlogin+'&key='
            +this.state.userkey+"&login_followed="
            +this.state.login).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.setState({type:"fd"});
                        this.setState({buttonvalue:"Unfollow",button2:false});
                    }else{
                        alert(resp.data.message);
                    }})
        }else if(this.state.type==="frfd"){
            axios.get('http://localhost:8080/hichem_kiki_3i017/RemoveFollower?login='
            +this.state.userlogin+'&key='
            +this.state.userkey+"&login_follower="
            +this.state.login).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.setState({type:"fd"});
                        this.setState({buttonvalue:"Unfollow",button2:false});
                    }else{
                        alert(resp.data.message);
                    }});
        }
    }

    handleClick2 = evt=>{
        if(this.state.type==="fr"){
            axios.get('http://localhost:8080/hichem_kiki_3i017/Follow?login='
            +this.state.userlogin+'&key='
            +this.state.userkey+"&login_followed="
            +this.state.login).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.setState({type:"frfd"});
                        this.setState({buttonvalue:"Remove Follower",button2:true,button2value:"Unfollow"});
                    }else{
                        alert(resp.data.message);
                    }})
        }else if(this.state.type==="frfd"){
            axios.get('http://localhost:8080/hichem_kiki_3i017/Unfollow?login='
            +this.state.userlogin+'&key='
            +this.state.userkey+"&login_followed="
            +this.state.login).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.setState({type:"fr"});
                        this.setState({buttonvalue:"Remove Follower",button2:true,button2value:"Follow"});
                    }else{
                        alert(resp.data.message);
                    }})
        }
    }





    render(){
        return(
            <div class="thumbnailcontainer">
                <b>{this.state.login}</b>
                <a href="#" onClick={this.consulterProfile}>Profile</a>
                <div class="columnimg">
                    <img src={require("./images/user.png")}/>
                    {/*<img src={require("state.imgurl")}/>*/}
                </div>
                <div class="columninfo">
                    <pre><b>{this.state.nom} {this.state.prenom}</b></pre>
                    <p>{this.state.age}</p>
                    <p>{this.state.sexe}
                    <input
                        type="submit"
                        value={this.state.buttonvalue}
                        class="thumbnailbutton"
                        onClick={this.handleClick1}/>{this.state.button2?<input
                                                        type="submit"
                                                        value={this.state.button2value}
                                                        class="thumbnailbutton"
                                                        onClick={this.handleClick2}/>:""}</p>
                </div>
            </div>
        )
    }
}
export default ProfileThumbnail;
