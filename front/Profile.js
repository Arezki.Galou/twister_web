import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import NavBar from "./NavBar";
import Post from "./Post";
import axios from 'axios';

class Profile extends Component{
  constructor(props){
    super(props);
    this.state={
        login: this.props.getLogin(),
        login_profile:this.props.getLogProf(),
        userkey:this.props.getKey(),
        nom:"",
        prenom:"",
        age:0,
        sexe:"",
        date:"",
        loginafficher:"",
        posts:[],
        nbposts:0,
        mode:0,
        editnom:"",
        editprenom:"",
        editage:"",
        editsexe:""
    }
    this.addPost=this.addPost.bind(this);
    this.reinitialistaion=this.reinitialistaion.bind(this);
    this.setEditMode=this.setEditMode.bind(this);
    this.setDelMode=this.setDelMode.bind(this);
    this.editProfile=this.editProfile.bind(this);
    this.deleteAccount=this.deleteAccount.bind(this);
    this.handleNomChange=this.handleNomChange.bind(this);
    this.handlePrenomChange=this.handlePrenomChange.bind(this);
    this.handleAgeChange=this.handleAgeChange.bind(this);
    this.handleSexeChangeToMale=this.handleSexeChangeToMale.bind(this);
    this.handleSexeChangeToFemale=this.handleSexeChangeToFemale.bind(this);

  }

  reinitialistaion(){
		this.setState({login:this.props.getLogin(),
			userkey:this.props.getKey(),
			posts:[],
			nbposts:0});
		this.fetchData();
	}

  fetchData(){
		axios.get('http://localhost:8080/hichem_kiki_3i017/Profile?login='
            +this.state.login+'&key='
            +this.state.userkey+'&login_profile='
            +this.state.login_profile).then(
      resp=>{
  			if(resp.data.status=="ok"){
					resp.data.postList.map( (c)=> {
							this.addPost(c);
					});
					this.setState({nbposts:this.state.nbposts+1,
                                    nom:resp.data.profile["nom"],
                                    editnom:resp.data.profile["nom"],
                                    prenom:resp.data.profile["prenom"],
                                    editprenom:resp.data.profile["prenom"],
                                    loginafficher:resp.data.profile["login"],
                                    age:resp.data.profile["age"],
                                    editage:resp.data.profile["age"],
                                    sexe:resp.data.profile["sexe"],
                                    editsexe:resp.data.profile["sexe"],
                                    date:resp.data.profile["date"]});
				}else{
					alert(JSON.stringify(resp.data, null, 4));
				}
      }
    );
	}

  addPost(post){
    this.state.posts.push(post)
    this.setState({nbposts:this.state.nbposts+1})
  }

	componentDidMount(){
		this.fetchData()
	}

    editProfile(){
        axios.get('http://localhost:8080/hichem_kiki_3i017/EditProfile?login='
        +this.props.getLogin()+'&key='
        +this.props.getKey()+'&new_nom='
        +this.state.editnom+"&new_prenom="
        +this.state.editprenom+"&new_sexe="
        +this.state.editsexe+"&new_age="
        +this.state.editage).then(
          resp=>{
            if(resp.data.status=="ok"){
                alert("Votre compte à été modifié");
                this.setState({mode:0,
                        nom:this.state.editnom,
                        prenom:this.state.editprenom,
                        age:this.state.editage,
                        sexe:this.state.editsexe})

              }else{
              alert(JSON.stringify(resp.data, null, 4));
          }});
      };

    setEditMode(){
        this.setState({mode:1})
    }

    setDelMode(){
        this.setState({mode:2})
    }

    deleteAccount(){
        axios.get('http://localhost:8080/hichem_kiki_3i017/DeleteAccount?login='
        +this.props.getLogin()+'&key='
        +this.props.getKey()+'&password='
        +this.state.password).then(
          resp=>{
            if(resp.data.status=="ok"){
                alert("Votre compte à été supprimé")
                this.props.setPageToLogin();
            }else{
              alert(JSON.stringify(resp.data, null, 4));
            }
          }
        );
    }

    handleNomChange = evt => {
        this.setState({ editnom: evt.target.value });
    }

    handlePrenomChange = evt => {
        this.setState({ editprenom: evt.target.value });
    }

    handleAgeChange = evt => {
        this.setState({ editage: evt.target.value });
    }

    handleSexeChangeToMale = evt => {
		this.setState({ editsexe: 1 });
	};
	handleSexeChangeToFemale = evt => {
		this.setState({ editsexe: 0 });
	};

    handlePasswordChange = evt => {
        this.setState({password:evt.target.value})
    }
    render(){
        return(
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2" >
                    </div>
                    <div class="col-lg-8 page" >
                        <div class="card">
                            <img src={require("./images/user.png")}/>
                            {this.state.mode==0?
                            <div class="infouser">
                                <h1>{this.state.loginafficher}</h1>
                                <p> {"Nom:"+this.state.nom}</p>
                                <p> {"Prenom:"+this.state.prenom}</p>
                                <p> {"Age:"+this.state.age}</p>
                                <p> {"Sexe:"}{this.state.sexe==1?"M":"F"}</p>
                                <p> {"Membre depuis :"+new Date(this.state.date).toDateString()}</p>
                                <input
                                    type="submit"
                                    value="Modifier"
                                    class="thumbnailbutton"
                                    onClick={this.setEditMode}/>
                                <input
                                    type="submit"
                                    value="Supprimer"
                                    class="thumbnailbutton"
                                    onClick={this.setDelMode}/>

                            </div>:this.state.mode==1?
                            <div class="infouser">
                                <h1>{this.state.loginafficher}</h1>
                                <div class="ids">
                                    <span>Nom</span>
                                    <input  type="text"
                                        value={this.state.editnom}
                                        onChange={this.handleNomChange}
                                        name="nom" />
                                </div>
                                <div class="ids">
                                    <span>Prenom</span>
                                    <input 	type="text"
                                        value={this.state.editprenom}
                                        onChange={this.handlePrenomChange}
                                        name="prenom" />
                                </div>
                                <div class="ids">
                                    <form><pre><input
                                                    type="radio"
                                                    name="gender"
                                                    value={this.state.editsexe}
                                                    onChange={this.handleSexeChangeToMale}
                                                    Checked/> Male <input
                                                                        type="radio"
                                                                        name="gender"
                                                                        value={this.state.editsexe}
                                                                        onChange={this.handleSexeChangeToFemale}/> Female	</pre>
                                    </form>
                                </div>
                                <div class="ids">
                                    <span>Age</span>
                                    <input 	type="number"
                                        name="age"
                                        value={this.state.editage}
                                        onChange={this.handleAgeChange}
                                        min="0"
                                        max="150"/>
                                </div>
                                <input
                                    type="submit"
                                    value="Annuler"
                                    class="thumbnailbutton"
                                    onClick={()=>this.setState({mode:0,
                                                                editnom:this.state.nom,
                                                                editprenom:this.state.prenom,
                                                                editsexe:this.state.sexe,
                                                                editage:this.state.age})}/>
                                <input
                                    type="submit"
                                    value="Soumettre"
                                    class="thumbnailbutton"
                                    onClick={this.editProfile}/>
                        </div>:<div>
                        <h3>Etes vous sur de vouloir supprimer votre compte?</h3>
                        <div class="ids">
                            <span>Password</span>
                            <input 	type="password"
                                value={this.state.password}
                                onChange={this.handlePasswordChange}
                                name="password" />
                        </div>
                        <input
                            type="submit"
                            value="Annuler"
                            class="thumbnailbutton"
                            onClick={()=>this.setState({mode:0,password:""})}/>
                        <input
                            type="submit"
                            value="Supprimer"
                            class="thumbnailbutton"
                            onClick={this.deleteAccount}/>
                        </div>}
        {this.state.posts.map( (c) => { if(c!=""){
                return 	<Post
                    childkey={this.state.userkey}
                    userlogin={this.state.login}
                    authorlogin={c.login}
                    postId={c.post_id}
                    text={c.text}
                    time={c.date}
                    imgurl={"some url"}
                    comList={c.comList}
                    nblikes={0}
                    liked={false}
                    reinit={this.reinitialistaion}/>
                }})}
                </div>
                </div>
                <div class="col-lg-2" >
                </div>
            </div>
        </div>)
    }
}
export default Profile;
