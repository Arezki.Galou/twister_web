import React, { Component } from 'react';
import Signin from './Signin';
import NavBar from './NavBar';

import NavigationPannel from './NavigationPannel';

import axios from 'axios';

class MainPage extends Component {

    constructor(props){
        super(props);
        this.state={page:"login",
                    isconnected:false,
                    login:"",
                    loginprof:"",
                    password:"",
                    userkey:""}
        this.setPageToSignin = this.setPageToSignin.bind(this);
        this.setPageToLogin = this.setPageToLogin.bind(this);
        this.setPageToLogout = this.setPageToLogout.bind(this);
        this.setPageToHome = this.setPageToHome.bind(this);
        this.setPageToProfile = this.setPageToProfile.bind(this);
        this.setPageToFollowers=this.setPageToFollowers.bind(this);
        this.setPageToFollowing=this.setPageToFollowing.bind(this);
        this.setPageToStats=this.setPageToStats.bind(this);
        this.setPageToSearch=this.setPageToSearch.bind(this);
        this.getLogin=this.getLogin.bind(this);
        this.getPage=this.getPage.bind(this);
        this.getKey=this.getKey.bind(this);
        this.getLoginProf=this.getLoginProf.bind(this);
        this.setProfile=this.setProfile.bind(this);
    }
    setProfile(data){
        this.setState({loginprof:data});
        this.setState({page:"profileUser"});
    }
    getLoginProf(){
        return this.state.loginprof;
    }
    getPage(){
        return this.state.page;
    }

    getLogin(){
        return this.state.login;
    }

    getKey(){
        return this.state.userkey;
    }

    setPageToLogin(){
        this.setState({page:"login"})
    }

    setPageToHome(){
        this.setState({page:"accueil"})
    }

    setPageToSignin(){
        this.setState({page:"signin"})
    }

    setPageToProfile(){
        this.setState({page:"profile"})
    }

    setPageToFollowers(){
        this.setState({page:"followers"})
    }

    setPageToStats(){
        this.setState({page:"stats"})
    }

    setPageToSearch(){
        this.setState({page:"search"})
    }

    setPageToFollowing(){
        this.setState({page:"following"})
    }
    setPageToLogout(){
        axios.get('http://localhost:8080/hichem_kiki_3i017/Logout?login='+this.state.login+'&key='+this.state.userkey).then(
            resp=>{
                if(resp.data.status=="ok"){
                    alert(resp.data.message);
                }else{
                    alert(resp.data.error+" : "+resp.data.message);
                }

            }
        );
        this.setPageToLogin();
    };

    handleLoginChange = evt => {
        this.setState({ login: evt.target.value ,loginprof:evt.target.value});
    };

    handlePasswordChange = evt => {
        this.setState({ password: evt.target.value });
    };
    canBeSubmitted() {
        const { login, password } = this.state;
        return (
            login.length > 0 &&
            password.length > 0
        );
    }
    handleSubmit = (evt) => {
        if (!this.canBeSubmitted()) {
            evt.preventDefault();
            return;
        }
        axios.get('http://localhost:8080/hichem_kiki_3i017/Login?login='+this.state.login
        +'&password='+this.state.password).then(
            resp=>{
                if(this.handleResponse(resp)){
                    alert("logged in");
                    this.setState({userkey:resp.data.key});
                    this.setPageToHome();
                }else{
                    alert(resp.data.message);
                }
            }
        );
    };

    handleResponse(resp){
        if(resp.data.status==="ok"){
            return true;
        }else{
            return false;
        }
    }

    render(){
        if(this.state.page==="login"){
            const isEnabled = this.canBeSubmitted();
            return(
                <div id="connexion_main" class="form">
                    <h1> Ouvrir une session </h1>
                    {/*<form>action="javascript:(function(){return;})()" method="get" onSubmit="javascript:connexion(this)"/>*/}
                    <div class="ids">
                        <span>Login</span>
                        <input  type="text"
                            placeholder="Enter login"
                            value={this.state.login}
                            onChange={this.handleLoginChange}
                            name="login"/>
                    </div>
                    <div class="ids">
                        <span>Mot de Passe</span>
                        <input type="password"
                            placeholder="Enter password"
                            value={this.state.password}
                            onChange={this.handlePasswordChange}
                            name="pass"/>
                    </div>
                    <div class="buttons">
                        <input
                            type="submit"
                            value="Connexion"
                            onClick={this.handleSubmit}
                            disabled={!isEnabled} />
                    </div>
                    <div class="links">
                        <div id="link2">
                            <input
                                type="submit"
                                value="Pas inscrit?"
                                onClick={()=>this.setPageToSignin()} />
                        </div>
                    </div>
                </div>
            )
        }else if(this.state.page==="signin"){
            return(
                <div className="MainPage">
                    <Signin setPageToLogin={this.setPageToLogin}/>
                </div>
            )
        }else{
            return(
                <div className="MainPage">
                    <nav>
                        <NavBar
                        setProfile={this.setProfile}
                        setPageToLogout={this.setPageToLogout}
                        setPageToHome={this.setPageToHome}
                        setPageToProfile={this.setPageToProfile}
                        setPageToFollowers={this.setPageToFollowers}
                        setPageToFollowing={this.setPageToFollowing}
                        setPageToStats={this.setPageToStats}
                        setPageToSearch={this.setPageToSearch}
                        getLogin={this.getLogin}/>
                    </nav>
                    <div>
                        <NavigationPannel
                           setProfile={this.setProfile}
                           getLoginProf={this.getLoginProf()}
                           getLogin={this.getLogin()}
                            getKey={this.getKey()}
                            getPage={this.getPage()}
                            setPageToLogin={this.setPageToLogin}
                            setPageToHome={this.setPageToHome}
                            setPageToProfile={this.setPageToProfile}
                            setPageToFollowers={this.setPageToFollowers}
                            setPageToFollowing={this.setPageToFollowing}
                            setPageToStats={this.setPageToStats}
                            setPageToSearch={this.setPageToSearch}/>
                    </div>
                </div>
            )
        }
    }
}
export default MainPage;
