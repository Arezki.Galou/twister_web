import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import axios from 'axios'

class AddComment extends Component{
  constructor(props){
    super(props)
    this.state={
                userkey:this.props.userkey,
                authorLogin:this.props.login,
                postId:this.props.postId,
                comId:this.props.comId,
                text:this.props.text,
                date:"",
                imgurl:this.props.imgurl,
                nblikes:this.props.nblikes,
                liked:false,
                nbcoms:0
              }
  }


  handleChange = evt => {
    this.setState({ text: evt.target.value });
  }

  enregistrer(){
    axios.get('http://localhost:8080/hichem_kiki_3i017/AddComment?login='
    +this.state.authorLogin+'&key='
    +this.state.userkey+'&post_id='
    +this.state.postId+'&text='+this.state.text).then(
    resp=>{
        if(resp.data.status=="ko"){
            alert(resp.data.message)
            //this.props.reinit();
        }else if(resp.data.status=="ok"){
            alert("Comment ajouté!")
            this.props.reinit();
        }
    }
);

    this.props.addComment(this.state)
    this.setState({ text: "" })
  }

  render(){
    return(
      <div className="AddComment" class="comcontainer">
        { this.state.authorLogin}
        <div class="columnimg">
        <img src={require("./images/user.png")}/>
        {/*<img src={require("state.imgurl")}/>*/}
        </div>
        <div class="columninfo">
            <textarea
                rows="2" cols="65"
                name="comment" form="usrform"
                value={this.state.text}
                onChange={this.handleChange}
                class="com" placeholder="Enter text here...">
            </textarea>
          <input  type="button" value="Enregistrer"
                  class="btn btn-info align-self-end mx-2"
                  onClick={()=>this.enregistrer()}
                  disabled={!this.state.text}/>
        </div>
      </div>
      )
  }
}
export default AddComment;
