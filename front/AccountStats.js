import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import NavBar from "./NavBar";
import Post from "./Post";
import axios from 'axios';

class AccountStats extends Component{
  constructor(props){
    super(props);
    this.state={ login: this.props.getLogin(),
                 userkey:this.props.getKey(),
                 nbposts:0,
                 nbcomments:0,
                 nblikes:0,
                 nbfollowing:0,
                 nbfollowers:0}
  }
  /*ajouter image dans render avant nom et prenom*/
  /*ajouter bouton en bas pour s'abonner si autre compte, modifier si c'est son compte*/


  fetchData(){
		axios.get('http://localhost:8080/hichem_kiki_3i017/AccountStats?login='+this.state.login
        +'&key='+this.state.userkey).then(
      resp=>{
        if(resp.data.status=="ok"){
					this.setState({
                       nbposts:resp.data.nbposts,
                       nbcomments:resp.data.nbcomments,
                       nblikes:resp.data.nblikes,
                       nbfollowing:resp.data.nbfollowing,
                       nbfollowers:resp.data.nbfollowers});
          {/*{"nbcomments":[3],"nblikes":[2],"nbfollowing":[3],"nbfollowers":[0],"nbposts":[2],"status":["ok"]}*/}
				}else{
					alert(JSON.stringify(resp.data, null, 4));
				}
      }
    );
	}

	componentDidMount(){
		this.fetchData()
	}

  render(){
    return(
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2" >
          </div>
          <div class="col-lg-8 page" >
            <div class="card">
            <pre>
              <ul>
                <li><b>Nombre de posts : {this.state.nbposts} </b></li>
                <li><b>Nombre de commentaires : {this.state.nbcomments} </b></li>
                <li><b>Nombre de likes : {this.state.nblikes} </b></li>
                <li><b>Nombre d'abonnés : {this.state.nbfollowers} </b></li>
                <li><b>Nombre d'abonnements : {this.state.nbfollowing} </b></li>
              </ul>
            </pre>
            </div>
          </div>
				  <div class="col-lg-2" >
				  </div>
				</div>
			</div>
  )
  }
}
export default AccountStats;
