import React, { Component } from 'react';
import Followers from './Followers';
import Following from './Following';
import Home from './Home';
import Profile from './Profile';
import AccountStats from './AccountStats';
import Search from './Search';
import ProfileUser from './ProfileUser';

class NavigationPannel extends Component {

    constructor(props){
        super(props);
        this.state={login:this.props.getLogin,
                    userkey:this.props.getKey,
                    login_profile:this.props.getLoginProf,
                    mapage:this.props.getPage}

        this.getKey=this.getKey.bind(this);
        this.getLogin=this.getLogin.bind(this);
        this.getLogProfile=this.getLogProfile.bind(this);
        this.setProfile1=this.setProfile1.bind(this);

    }
    getKey(){
      return this.state.userkey;
    }
    getLogin(){
      return this.state.login;
    }
  getLogProfile(){
      return this.state.login_profile;
  }
  setProfile1(data){
      this.setState({login_profile:data});
      this.props.setProfile(data);
  }


    render(){
        if(this.props.getPage === "profile"){
            return(
                <div className="MainPage">  <h1> Profile  </h1>
                    <Profile
                        getKey={this.getKey}
                        getLogin={this.getLogin}
                        getLogProf={this.getLogin}
                        setPageToLogin={this.props.setPageToLogin}/>
                </div>
            )
        }else if(this.props.getPage === "followers"){
            return(
                <div className="MainPage"> <h1> Abonnés</h1>
                    <Followers
                        setProfile1={this.setProfile1}
                        getKey={this.getKey}
                        getLogin={this.getLogin}/>
                </div>
            )
        }else if(this.props.getPage === "following"){
            return(<div className="MainPage"> <h1> Abonnements</h1>
                <Following
                    setProfile1={this.setProfile1}
                    getKey={this.getKey}
                    getLogin={this.getLogin}/>
                </div>
            )
        }else if(this.props.getPage === "accueil"){
            return(
                <div className="MainPage"> <h1>{this.state.page}</h1>
                    <Home
                        getKey={this.getKey}
                        getLogin={this.getLogin}/>
                </div>
            )
        }else if(this.props.getPage === "stats"){
            return(
                <div className="MainPage"> <h1>{this.state.page}</h1>
                    <AccountStats
                        getKey={this.getKey}
                        getLogin={this.getLogin}/>
                </div>
            )
        }else if(this.props.getPage === "search"){
            return(
                <div className="MainPage"> <h1>{this.state.page}</h1>
                    <Search
                        setProfile1={this.setProfile1}
                        getKey={this.getKey}
                        getLogin={this.getLogin}/>
                </div>
            )
        }else if(this.props.getPage==="profileUser"){return(<div> <ProfileUser

                                                                   getKey={this.getKey}
                                                                   getLogin={this.getLogin}
                                                                   getLogProf={this.getLogProfile}
                                                                                />

                                                                </div>)
                                                 }
    }
}
export default NavigationPannel;
