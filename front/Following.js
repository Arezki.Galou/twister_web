import React, { Component } from 'react';
import ProfileThumbnail from './ProfileThumbnail';
import NavBar from './NavBar';
import axios from 'axios';

class Following extends Component{

  constructor(props){
    super(props)
    this.state={list:[],
      userkey:this.props.getKey(),
      login:this.props.getLogin(),
                       nbf:0}
    this.ajout=this.ajout.bind(this)
  this.setProfile4=this.setProfile4.bind(this);
}
  setProfile4(data){
      this.props.setProfile1(data);
  }

  ajout(pers){
    this.state.list.push(pers)
    this.setState({nbf:this.state.nbf+1})
  }

  fetchData(){
    axios.get('http://localhost:8080/hichem_kiki_3i017/GetUserFollowing?login='+this.props.getLogin()+'&key='+this.props.getKey()).then(
      resp=>{
        if(resp.data.status=="ok"){
          resp.data.followingList.map( (c)=> {
              this.ajout(c);
          });
        }else{
          alert(JSON.stringify(resp.data, null, 4));
        }
      }
    );
  }

  componentDidMount(){
    this.fetchData()
  }


  render(){
    return(
      <div className="Following">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-2" >
            </div>
            <div class="col-lg-8 page" >
            {/*<ProfileThumbnail	nom="n1"
                                                          prenom="p1"
                                                          age="a1"
                                                          sexe="s1"
                                                          occupation="o1"/>*/}
              {
                  this.state.list.map( (c) => { if(c!=""){
                    console.log("1 "+c.login);
                    return <ProfileThumbnail
                                setProfilef={this.setProfile4}
                                userlogin={this.state.login}
                                userkey={this.state.userkey}
                                profilelogin={c.login}
                                nom={c.nom}
                                prenom={c.prenom}
                                type={c.type}
                                age={c.age}
                                sexe={c.sexe}
                                occupation={c.occupation}/>}
                                }
                            )
            }

            </div>
            <div class="col-lg-2" >
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default Following;
