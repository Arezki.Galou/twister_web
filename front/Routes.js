import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Login from './Login';
import Signin from './Signin';
import Home from './Home';
import Followers from './Followers';
import Following from './Following';

const createRoutes = () => (
    <BrowserRouter>
      <Route exact path="/Login" component={Login}/>
      <Route exact path="/Signin" component={Signin}/>
      <Route exact path="/Home" component={Home}/>
      <Route exact path="/Followers" component={Followers}/>
      <Route exact path="/Following" component={Following}/>
    </BrowserRouter>
);

export default createRoutes;
