import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import 'bootstrap/dist/css/bootstrap.css';
import {Nav, Navbar} from 'react-bootstrap';
import Axios from 'axios';
import Logout from './Logout';
class NavBar extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
        <div class="topnav">
          <div class="topnav-centered">
            <a href="#" class="active" onClick={this.props.setPageToHome}>Accueil</a>
          </div>
          <a href="#" onClick={this.props.setPageToProfile}>Profil</a>
          <a href="#" onClick={this.props.setPageToFollowing}>Abonnements</a>
          <a href="#" onClick={this.props.setPageToFollowers}>Abonnés</a>
          <div class="topnav-right">
            <a href="#" onClick={this.props.setPageToStats}>Stats</a>
            <a href="#" onClick={this.props.setPageToSearch}>Search</a>
            <a href="#" onClick={this.props.setPageToLogout}>Déconnexion</a>
          </div>
        </div>
    );
  }
}

export default NavBar;
