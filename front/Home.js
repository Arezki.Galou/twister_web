import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import mycssfile from "./mycssfile.css";
import NavBar from "./NavBar";
import AddPost from "./AddPost";
import AddComment from "./AddComment";
import Comment from "./Comment";
import Post from "./Post";
import axios from 'axios';

class Home extends Component{

	constructor(props){
		super(props);
		this.state={login:this.props.getLogin(),
			userkey:this.props.getKey(),
			posts:[],
			nbposts:0};
			this.addPost=this.addPost.bind(this);
			this.fetchData=this.fetchData.bind(this);
			this.reinit=this.reinit.bind(this);
	}

	reinit(){
		this.setState({login:this.props.getLogin(),
			userkey:this.props.getKey(),
			posts:[],
			nbposts:0});
		this.fetchData();
	}

	addPost(post){
		this.state.posts.push(post);
		//this.state.posts.splice(0,0,post);
		//let newl=[post]
		//let newP=newl.concat(this.state.posts)
		this.setState({nbposts:this.state.nbposts+1})
	}

	removePost(post){
		this.state.posts.splice( this.state.posts.indexOf(post), 1 );
		this.setState({nbposts:this.state.nbposts-1})
	}

	fetchData(){
		axios.get('http://localhost:8080/hichem_kiki_3i017/Home?login='
		+this.state.login
		+'&key='+this.state.userkey).then(
			resp=>{
				if(resp.data.status=="ok"){
					resp.data.postList.map( (c)=> {
						this.addPost(c);
					});
				}else{
					alert(JSON.stringify(resp.data, null, 4));
				}
			}
		);
	}

	componentDidMount(){
		this.fetchData()
	}

	render(){
		return(
			<div className="Home">
			<div class="container-fluid">
			<div class="row">
			<div class="col-lg-2" >
			</div>
			<div class="col-lg-8 page" >
			{/*this.state.userkey*/}
			<AddPost
				addPost={this.addPost}
				login={this.state.login}
				childkey={this.state.userkey}
				reinit={this.reinit}/>
			{/*Affichage de la liste des postes*/}
			{
				this.state.posts.map( (c,i) => { if(c!=""){
				console.log(c.likeList)
				return(
					<Post
						childkey={this.state.userkey}
						userlogin={this.state.login}
						authorlogin={c.login}
						postId={c.post_id}
						text={c.text}
						time={c.date}
						imgurl={"some url"}
						comList={c.comList}
						likeList={c.likeList}
						nblikes={0}
						liked={false}
						reinit={this.reinit}/>
				)
				}})
			}
			</div>
			<div class="col-lg-2" >
			</div>
			</div>
			</div>
			</div>
		)
	}
}
export default Home;
