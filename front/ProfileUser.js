import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import NavBar from "./NavBar";
import Post from "./Post";
import axios from 'axios';

class ProfileUser extends Component{
  constructor(props){
    super(props);
    this.state={ login: this.props.getLogin(),
                 login_profile:this.props.getLogProf(),
                 userkey:this.props.getKey(),
                 nom:"",
                 prenom:"",
                 age:"",
                 sexe:"",
                 date:"",
                 loginafficher:"",
                 posts:[],
                 nbposts:0}
    this.addPost=this.addPost.bind(this);
    this.reinitialistaion=this.reinitialistaion.bind(this);
  }
  /*ajouter image dans render avant nom et prenom*/
  /*ajouter bouton en bas pour s'abonner si autre compte, modifier si c'est son compte*/

  reinitialistaion(){
		this.setState({login:this.props.getLogin(),
			userkey:this.props.getKey(),
			posts:[],
			nbposts:0});
		this.fetchData();
	}

  fetchData(){
		axios.get('http://localhost:8080/hichem_kiki_3i017/Profile?login='
            +this.state.login+'&key='
            +this.state.userkey+'&login_profile='
            +this.state.login_profile).then(
      resp=>{
  			if(resp.data.status=="ok"){
					resp.data.postList.map( (c)=> {
							this.addPost(c);
					});
					this.setState({nbposts:this.state.nbposts+1,nom:resp.data.profile["nom"],prenom:resp.data.profile["prenom"],loginafficher:resp.data.profile["login"],age:resp.data.profile["age"],sexe:resp.data.profile["sexe"],date:resp.data.profile["date"]});
				}else{
					alert(JSON.stringify(resp.data, null, 4));
				}
      }
    );
	}

  addPost(post){
    this.state.posts.push(post)
    this.setState({nbposts:this.state.nbposts+1})
  }

	componentDidMount(){
		this.fetchData()
	}

  render(){
    return(
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2" >
          </div>
          <div class="col-lg-8 page" >
            <div class="card">
              <img src={require("./images/user.png")}/>
              <div class="infouser">
              <h1>{this.state.loginafficher}</h1>
              <p> {"Nom:"+this.state.nom}</p>
              <p> {"Prenom:"+this.state.prenom}</p>
              <p> {"Age:"+this.state.age}</p>
              <p> {"Sexe:"+this.state.sexe}</p>
              <p> {"Membre depuis :"+new Date(this.state.date).toDateString()}</p>
              </div>
            {this.state.posts.map( (c) => { if(c!=""){
                                            return 	<Post
                                            childkey={this.state.userkey}
                                            userlogin={this.state.login}
                                            authorlogin={c.login}
                                            postId={c.post_id}
                                            text={c.text}
                                            time={c.date}
                                            imgurl={"some url"}
                                            comList={c.comList}
                                            nblikes={0}
                                            liked={false}
                                            reinit={this.reinitialistaion}
                                                   />
                                          }}
                                 )
            }</div>
          </div>
				  <div class="col-lg-2" >
				  </div>
				</div>
			</div>
  )
  }
}
export default ProfileUser;
