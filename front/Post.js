import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import AddComment from "./AddComment";
import Comment from "./Comment";
import axios from 'axios';

class Post extends Component{
    /*
    Page ne contenant qu'un seul poste avec ses commentaires
    Doit apparaitre quand on clique sur un poste dans la page Home ou dans une Profile page
    Permet d'ajouter un commentaire de liker
    */
    constructor(props){
        super(props)
        this.state={
            login:this.props.userlogin,
            userkey:this.props.childkey,
            authorlogin:this.props.authorlogin,
            postId:this.props.postId,
            text:this.props.text,
            edittext:"",
            time:this.props.time,
            imgurl:this.props.imgurl,
            comments:this.props.comList,
            likes:this.props.likeList,
            nblikes:this.props.likeList?this.props.likeList.length:0,
            liked:false,
            nbcoms:0,
            editmode:false}
        if(this.state.likes) this.state.likes.map((c)=>{ if(c.login==this.state.login) this.state.liked=true})
        this.addComment=this.addComment.bind(this)
        this.clickLike=this.clickLike.bind(this)
        this.render=this.render.bind(this)
        this.setEditMode=this.setEditMode.bind(this)
        this.setNormalMode=this.setNormalMode.bind(this)
        this.deletePost=this.deletePost.bind(this)
    }

    clickLike(){
        if(!this.state.liked){
            axios.get('http://localhost:8080/hichem_kiki_3i017/Like?login='
                +this.state.login+'&key='
                +this.state.userkey+'&post_id='
                +this.state.postId).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.props.reinit();
                    }else{
                        alert(resp.data.message);
                    }
                }
            );
        }else{
            axios.get('http://localhost:8080/hichem_kiki_3i017/Dislike?login='
                +this.state.login+'&key='
                +this.state.userkey+'&post_id='
                +this.state.postId).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.props.reinit();
                    }else{
                        alert(resp.data.message)
                    }
                }
            );
        }
    }


    editPost(){
        if(this.state.edittext=="  "){
            alert("vous ne pouvez pas inserer un post vide");
        }else{
            /*  this.props.addPost(this.state)
            this.setState({ text: "" })*/
            axios.get('http://localhost:8080/hichem_kiki_3i017/EditPost?login='
                +this.state.login+'&key='
                +this.state.userkey+'&text='
                +this.state.edittext+'&post_id='
                +this.state.postId).then(
                resp=>{
                    if(resp.data.status=="ko"){
                        alert(resp.data.message)
                        this.props.reinit();
                        this.setNormalMode();
                        this.setState({edittext:""});
                    }else if(resp.data.status=="ok"){
                        alert("Post Modifié!")
                        this.props.reinit();
                        this.setNormalMode();
                        this.setState({edittext:""});
                    }
                }
            );
        }
    }

    addComment(comment){
        this.state.comments.push(comment)
        this.setState({nbcoms:this.state.comments.length})
    }

    deletePost(){
        axios.get('http://localhost:8080/hichem_kiki_3i017/RemovePost?login='
            +this.state.login+'&key='
            +this.state.userkey+'&post_id='
            +this.state.postId).then(
            resp=>{
                if(resp.data.status=="ko"){
                    alert(resp.data.message)
                    this.props.reinit();
                }else if(resp.data.status=="ok"){
                    alert("Post Supprimé!")
                    this.props.reinit();
                }
            }
        );
    }




    handleChange = evt => {
        this.setState({ edittext: evt.target.value });
    }

    setEditMode(){
        this.setState({editmode:true})
    }

    setNormalMode(){
        this.setState({editmode:false})
    }

    render(){
        if(this.state.editmode==false){
            return(
                <div>
                <div class="postcontainer">
                {this.state.authorlogin}
                <div class="columnimg">
                <img src={require("./images/user.png")}/>
                {/*<img src={require("state.imgurl")}/>*/}
                </div>
                {this.state.authorlogin===this.state.login?
                <div class="dropdown">
                    <button class="dropbtn">&#9660;</button>
                    <div class="dropdown-content">
                        <a href="#" onClick={()=>this.setEditMode()}>Modifier</a>
                        <a href="#" onClick={()=>this.deletePost()}>Supprimer</a>
                    </div>
                </div>:""}
                <a href="#" class="like" onClick={()=>this.clickLike()}>Like!
                {this.state.nblikes>0?<b>x{this.state.nblikes}</b>:" "}</a>
                <div class="columninfo">
                <p> {this.state.text} </p>
                {/*<span class="time-right">12:00</span>*/}
                <span class="time-right">{new Date(this.state.time["$date"]).toGMTString()}</span>
                {/*<span class="time-right">{this.state.time}</span>*/}
                </div>
                </div>
                {/*Affichage des commentaire*/}
                {this.state.comments.map( (c) => {
                    if(c!=""){
                        console.log('shit'+ c.comment_id)
                        return <Comment
                                    keychild={this.state.userkey}
                                    login={this.state.login}
                                    authorlogin={c.login}
                                    postId={this.state.post_id}
                                    comId={c.comment_id}
                                    text={c.text}
                                    time={c.date}
                                    imgurl={"some url"}
                                    likes={c.likeList}
                                    nblikes={0}
                                    liked={false}
                                    reinit={this.props.reinit}/>
                    }})
                }
                {/*afficher tous les commentaires de commentaires*/}
                <AddComment
                    addComment={this.addComment}
                    reinit={this.props.reinit}
                    userkey={this.state.userkey}
                    login={this.state.login}
                    postId={this.state.postId}/>
                </div>
            )
        }else{
            return(
                <div className="AddPost" class="postcontainer">
                  {this.state.authorlogin}
                  <div class="columnimg">
                  <img src={require("./images/user.png")}/>
                  {/*<img src={require("state.imgurl")}/>*/}
                  </div>
                  <div class="columninfo">
                    <textarea
                        rows="3" cols="75"
                        name="comment" form="usrform"
                        value={this.state.edittext}
                        onChange={this.handleChange}
                        class="com" placeholder="Enter text here...">
                    </textarea>
                    <input
                        type="button"
                        onClick={()=>this.editPost()}
                        value="Enregistrer"
                        class="btn btn-info align-self-end mx-2"/>
                    <input
                        type="button"
                        onClick={()=>this.setNormalMode()}
                        value="Annuler"
                        class="btn btn-info align-self-end mx-2"/>
                  </div>
                </div>
            )
        }
    }
}
export default Post;
