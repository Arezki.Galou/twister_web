import React, { Component } from 'react';
import mycssfile from "./mycssfile.css";
import axios from 'axios';

class Comment extends Component{
    constructor(props){
        super(props)
        this.state={
            userkey:this.props.keychild,
            login:this.props.login,
            authorlogin:this.props.authorlogin,
            postId:this.props.postId,
            comId:this.props.comId,
            text:this.props.text,
            edittext:"",
            time:this.props.time,
            imgurl:this.props.imgurl,
            nblikes:this.props.likes?this.props.likes.length:0,
            likes:this.props.likes,
            liked:false,
            editmode:false}
        if(this.state.likes) this.state.likes.map((c)=>{ if(c.login==this.state.login) this.state.liked=true})
        this.clickLike=this.clickLike.bind(this)
        this.handleChange=this.handleChange.bind(this)
        this.setEditMode=this.setEditMode.bind(this)
        this.setNormalMode=this.setNormalMode.bind(this)
        this.editComment=this.editComment.bind(this)
        this.deleteComment=this.deleteComment.bind(this);
    }

    clickLike(){
        if(!this.state.liked){
            axios.get('http://localhost:8080/hichem_kiki_3i017/Like?login='
                +this.state.login+'&key='
                +this.state.userkey+'&comment_id='
                +this.state.comId).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.props.reinit();
                    }else{
                        alert(resp.data.message);
                    }
                }
            );
        }else{
            axios.get('http://localhost:8080/hichem_kiki_3i017/Dislike?login='
                +this.state.login+'&key='
                +this.state.userkey+'&comment_id='
                +this.state.comId).then(
                resp=>{
                    if(resp.data.status=="ok"){
                        this.props.reinit();
                    }else{
                        alert(resp.data.message)
                    }
                }
            );
        }
    }

    handleChange = evt => {
        this.setState({ edittext: evt.target.value });
    }

    setEditMode(){
        this.setState({editmode:true})
    }

    setNormalMode(){
        this.setState({editmode:false})
    }

    editComment(){
        if(this.state.edittext=="  "){
            alert("vous ne pouvez pas inserer un commentaire vide");
        }else{
            /*  this.props.addPost(this.state)
            this.setState({ text: "" })*/
            axios.get('http://localhost:8080/hichem_kiki_3i017/EditComment?login='
                +this.state.login+'&key='
                +this.state.userkey+'&text='
                +this.state.edittext+'&comment_id='
                +this.state.comId).then(
                resp=>{
                    if(resp.data.status=="ko"){
                        alert(resp.data.message)
                        this.props.reinit();
                        this.setNormalMode();
                        this.setState({edittext:""});
                    }else if(resp.data.status=="ok"){
                        alert("Commentaire Modifié!")
                        this.props.reinit();
                        this.setNormalMode();
                        this.setState({edittext:""});
                    }
                }
            );
        }
    }

    deleteComment(){
        axios.get('http://localhost:8080/hichem_kiki_3i017/RemoveComment?login='
            +this.state.login+'&key='
            +this.state.userkey+'&comment_id='
            +this.state.comId).then(
            resp=>{
                if(resp.data.status=="ko"){
                    alert(resp.data.message)
                    this.props.reinit();
                }else if(resp.data.status=="ok"){
                    alert("Comment Supprimé!")
                    this.props.reinit();
                }
            }
        );
    }


    render(){

        if(this.state.editmode==false){
            return(
                <div className="Comment" class="comcontainer">
                    {/*{this.state.authorN} {this.state.authorP}*/}
                    {this.state.authorlogin}
                    <div class="columnimg">
                        <img src={require("./images/user.png")}/>
                        {/*<img src={require("state.imgurl")}/>*/}
                    </div>
                    {this.state.authorlogin===this.state.login?
                    <div class="dropdown">
                        <button class="dropbtn">&#9660;</button>
                        <div class="dropdown-content">
                            <a href="#" onClick={()=>this.setEditMode()}>Modifier</a>
                            <a href="#" onClick={()=>this.deleteComment()}>Supprimer</a>
                        </div>
                    </div>:""}
                    <a  href="#" class="like"
                        onClick={()=>this.clickLike()}>Like!{this.state.nblikes>0?<b>x{this.state.nblikes}</b>:""}
                    </a>
                    <div class="columninfo">
                        <p> {this.state.text} </p>
                        <span class="time-right">{new Date(this.state.time["$date"]).toGMTString()}</span>
                        {/*<p> {this.state.text} </p>*/}
                        {/*<span class="time-right">{this.state.time}</span>*/}
                    </div>
                </div>
            )
        }else{
            return(
                <div className="AddPost" class="postcontainer">
                    {this.state.authorlogin}
                    <div class="columnimg">
                        <img src={require("./images/user.png")}/>
                        {/*<img src={require("state.imgurl")}/>*/}
                    </div>
                    <div class="columninfo">
                        <textarea
                            rows="2" cols="65"
                            name="comment" form="usrform"
                            value={this.state.edittext}
                            onChange={this.handleChange}
                            class="com" placeholder="Enter text here...">
                        </textarea>
                        <input
                            type="button"
                            onClick={()=>this.editComment()}
                            value="Enregistrer"
                            class="btn btn-info align-self-end mx-2"/>
                        <input
                            type="button"
                            onClick={()=>this.setNormalMode()}
                            value="Annuler"
                            class="btn btn-info align-self-end mx-2"/>
                    </div>
                </div>
            )
        }
    }
}
export default Comment;
