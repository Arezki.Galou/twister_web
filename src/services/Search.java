package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import tools.Database;
import tools.UserTools;

public class Search {

	public static JSONObject searchUsers(String login, String key, String query) {
		if (login == null || key == null) {
			return Signal.serviceRefused(100);
		}
		if (!tools.UserTools.userExists(login)) {
			return Signal.serviceRefused(202);
		}
		if (!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login, tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);
		}
		Connection cn;
		Statement st;
		String requete;
		JSONObject resp = new JSONObject();
		JSONObject obj;
		int val;
		int cpt = 0;
		ResultSet res;
		String[] wordList = query.split("\\s+");
		List<Integer> selected = new ArrayList<Integer>();
		List<Integer> following = new ArrayList<Integer>();
		List<Integer> followers = new ArrayList<Integer>();
		int id = Integer.valueOf(tools.UserTools.getId(login));
		try {
			resp.accumulate("Users", "");
			resp.accumulate("Users", "");
		} catch (JSONException e1) {
			e1.printStackTrace();
			return services.Signal.serviceRefused(401);
		}
		try {
			cn = Database.getMySQLConnection();
			st = cn.createStatement();
			requete = "Select * from Followers";
			res = st.executeQuery(requete);
			while (res.next()) {
				if (id == res.getInt("followed")) {
					followers.add(res.getInt("follower"));
				} else if (id == res.getInt("follower")) {
					following.add(res.getInt("followed"));
				}

			}
			for (String word : wordList) {
				requete = "Select * from User where login like \"%" + word + "%\" " + "or nom like \"%" + word
						+ "%\" or prenom like \"%" + word + "%\";";
				res = st.executeQuery(requete);
				while (res.next()) {
					obj = new JSONObject();
					try {
						val = res.getInt("id");
						if (!selected.contains(val)) {
							selected.add(val);
							obj.put("login", res.getString("login"));
							obj.put("nom", res.getString("nom"));
							obj.put("prenom", res.getString("prenom"));
							obj.put("sexe", res.getString("sexe"));
							obj.put("age", res.getString("age"));

							if (val == id) {
								continue;
							} else if (following.contains(val) && followers.contains(val)) {
								obj.put("type", "frfd");
								resp.accumulate("Users", obj);
								cpt++;
							} else if (following.contains(val)) {
								obj.put("type", "fd");
								resp.accumulate("Users", obj);
								cpt++;
							} else if (followers.contains(val)) {
								obj.put("type", "fr");
								resp.accumulate("Users", obj);
								cpt++;
							} else {
								obj.put("type", "o");
								resp.accumulate("Users", obj);
								cpt++;
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
						return services.Signal.serviceRefused(401);
					}
				}
			}
			try {
				resp.put("status", "ok");
				resp.put("nbr", cpt);
			} catch (JSONException e) {
				e.printStackTrace();
				return services.Signal.serviceRefused(401);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(301);
		}
		return resp;
	}

	public static JSONObject search(String login, String key, String query) {
		if (login == null || key == null || query == null) {
			return Signal.serviceRefused(100);
		}
		if (!tools.UserTools.userExists(login)) {
			return Signal.serviceRefused(202);
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);
		}
		if (!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login, tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}

		ArrayList<String> postIdList;
		String str;
		try {
			str = tools.MapReduce.recherche(query).getString("postList");
			str = str.substring(1, str.length() - 1);
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		} catch (SQLException e) {
			e.printStackTrace();
			return Signal.serviceRefused(301);
		}
		postIdList = new ArrayList<String>(Arrays.asList(str.split(",")));// list des _id des posts
		int id = Integer.parseInt(tools.UserTools.getId(login));

		ArrayList<Integer> f = new ArrayList<Integer>();
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st = cn.createStatement();
			f.add(id);
			String requete = "Select * From Followers Where follower=" + id + ";";
			ResultSet res = st.executeQuery(requete);
			while (res.next()) {
				f.add(res.getInt("followed"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(401);
		}

		com.mongodb.client.MongoClient mongo = MongoClients.create();
		MongoDatabase db = mongo.getDatabase("3i017");

		MongoCollection<Document> mcPosts = db.getCollection("Posts");
		MongoCollection<Document> mcComments = db.getCollection("Comments");
		MongoCollection<Document> mcLikes = db.getCollection("Likes");

		Document qPosts = new Document();
		Document qLikes = new Document();
		Document qComments = new Document();
		Document qLikesComments = new Document();

		MongoCursor<Document> cPosts;
		MongoCursor<Document> cLikes;
		MongoCursor<Document> cComments;
		MongoCursor<Document> cLikesComments;

		ArrayList<JSONObject> likeList = new ArrayList<JSONObject>();
		ArrayList<JSONObject> postList = new ArrayList<JSONObject>();
		ArrayList<JSONObject> comList = new ArrayList<JSONObject>();
		ArrayList<String> selected = new ArrayList<String>();
		JSONObject post = new JSONObject();
		JSONObject comment = new JSONObject();
		Document tmp;

		try {
			for (String i : postIdList) {
				qPosts = new Document();

				qPosts.append("_id", new ObjectId(i.substring(1, i.length() - 1)));
				cPosts = mcPosts.find(qPosts).iterator();
				if (!cPosts.hasNext()) {
					qComments = new Document();
					qComments.append("_id", new ObjectId(i.substring(1, i.length() - 1)));
					cComments = mcComments.find(qComments).iterator();
					if (!cComments.hasNext()) {
						continue;
					}
					tmp = cComments.next();
					comment = new JSONObject(tmp.toJson());
					qPosts = new Document();
					qPosts.put("post_id", comment.getString("post_id"));
					cPosts = mcPosts.find(qPosts).iterator();
				}
				while (cPosts.hasNext()) {
					tmp = cPosts.next();
					post = new JSONObject(tmp.toJson());
					if (selected.contains(post.getString("post_id"))) {
						continue;
					}
					selected.add(post.getString("post_id"));
					if (f.contains(post.getInt("user_id"))) {
						qLikes = new Document();
						qLikes.append("post_id", post.getString("post_id"));
						cLikes = mcLikes.find(qLikes).iterator();
						likeList = new ArrayList<JSONObject>();
						while (cLikes.hasNext()) {
							likeList.add(new JSONObject(cLikes.next().toJson()));
						}
					}
					post.put("likeList", likeList);
					qComments = new Document();
					qComments.append("post_id", post.getString("post_id"));
					cComments = mcComments.find(qComments).iterator();
					comList = new ArrayList<JSONObject>();
					while (cComments.hasNext()) {
						tmp = cComments.next();
						comment = new JSONObject(tmp.toJson());
						String comment_id = tmp.getString("comment_id");
						qLikesComments = new Document();
						qLikesComments.append("comment_id", comment_id);
						cLikesComments = mcLikes.find(qLikesComments).iterator();
						likeList = new ArrayList<JSONObject>();
						likeList = new ArrayList<JSONObject>();
						while (cLikesComments.hasNext()) {
							likeList.add(new JSONObject(cLikesComments.next().toJson()));
						}
						comment.put("likeList", likeList);
						comList.add(comment);
					}
					post.put("comList", comList);
					postList.add(post);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(401);
		}
		JSONObject res = new JSONObject();
		try {
			res.put("postList", postList);
			res.put("status", "ok");
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return res;
	}
}
