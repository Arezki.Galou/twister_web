package services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import tools.UserTools;

public class Posts {

	public static JSONObject addPost(String login,String key, String text) {
		if(login==null||key==null||text==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		int user_id=Integer.parseInt(tools.UserTools.getId(login));
		String post_id="";
		LocalDateTime date = LocalDateTime.now();
		post_id=Integer.toString(user_id)+date; 

		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mc=db.getCollection("Posts");

		Document d=new Document();
		d.append("user_id",user_id);
		d.append("login", login);
		d.append("post_id",post_id);
		d.append("date", date);
		d.append("text", text);
		mc.insertOne(d);
		JSONObject resp=new JSONObject();
		try {
			resp.put("status", "ok");
			resp.put("post_id",post_id);
			resp.put("date", date);
		} catch (JSONException e1) {
			return Signal.serviceRefused(401);
		}
		return resp;
	}


	public static JSONObject removePost(String login,String key,String post_id) {
		if(login==null||key==null||post_id==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}

		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mcPosts=db.getCollection("Posts");
		MongoCollection<Document> mcComments=db.getCollection("Comments");
		MongoCollection<Document> mcLikes=db.getCollection("Likes");
		Document d=new Document();
		Document q;
		
		d.append("post_id",post_id);
		
		mcLikes.deleteMany(d);
		MongoCursor<Document> cursor=mcComments.find(d).iterator();
		List<Document> comments = (List<Document>) mcComments.find(d).into(new ArrayList<Document>());
		for(Document c:comments) {
			q=new Document();
			q.put("comment_id", c.getString("comment_id"));
			mcLikes.deleteMany(q);
			mcComments.deleteOne(q);
		}
		
		cursor=mcPosts.find(d).iterator();
		boolean trouve=false;
		while(cursor.hasNext()) {
			Document obj=cursor.next();
			trouve=true;
			mcPosts.deleteOne(obj);
		}
		if (trouve) {
			JSONObject resp=new JSONObject();
			try {
				resp.put("status", "ok");
			} catch (JSONException e1) {
				e1.printStackTrace();
				return Signal.serviceRefused(401);
			}
			return resp;
		} else {
			return services.Signal.serviceRefused(304);
		}
	}
	
	public static JSONObject editPost(String login,String key,String post_id,String text) {
		if(login==null||key==null||post_id==null||text==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mc=db.getCollection("Posts");

		Document q=new Document();
		q.put("post_id", post_id);		
		Document upd=new Document();
		upd.append("text", text);
		Document updOp=new Document();
		updOp.append("$set", upd);

		mc.updateOne(q, updOp);

		JSONObject resp=new JSONObject();
		try {
			resp.append("status", "ok");
			resp.append("message", "Votre post à été modifié");
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return resp;
	}	

}
