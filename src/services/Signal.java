package services;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class Signal {

	private static Map<Integer, String> errmap = new HashMap<Integer, String>() {
		
		private static final long serialVersionUID = 1L;
		{
			put(1, "Erreur inattendue");
			
			put(100, "Paramétres manquants!");
			
			put(201, "Le login existe deja!");
			put(202, "Le login n'existe pas!");
			put(203, "Mot de passe incorrecte!");
			
			put(204, "Erreur lors de l'inscription!");
			put(205, "L'utilisateur est déjà connecté!");
			put(206, "L'utilisateur est déjà déconnecté!");
			put(207, "Erreur lors de la déconnenxion!");
			
			put(208, "Votre session a expiré!");
			put(209, "Un des utilisateurs n'existe pas!");
			
			put(210, "On ne peut pas s'abonner à soit même!");
			put(211, "L'abonnement est déjà effectué!");
			put(212, "L'abonnement ne s'est pas effectué!");
			put(213, "Erreur lors de la suppression de l'abonnement!");
			put(214, "Erreur lors de la modification du profil!");
			put(215, "Erreur lors de la suppression du compte!");
			
			put(301, "Erreur SQL!");
			put(302, "Erreur MongoDB!");
			
			put(303, "Le commentaire n'existe pas!");
			put(304, "Le post n'existe pas!");
			put(305, "Clé invalide!");
			put(306, "Le like n'a pas été trouvé!");
			put(307, "Le post est dupliqué!");
	
			
			put(401, "Erreur JSON!");
		}
	};
	
	public static JSONObject serviceRefused(int codeErreur) {
		JSONObject ret = new JSONObject();
		try {
			ret.put("status", "ko");
			ret.put("error",codeErreur);
			ret.put("message",errmap.get(codeErreur));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ret;
	}

}