package services;


import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import tools.UserTools;

public class Likes {
	
	
	public static JSONObject like(String login,String key,String post_id,String comment_id) {
		if(login==null||key==null||(post_id==null && comment_id==null)) {
			return Signal.serviceRefused(100);	
		}	
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
		
		Document q=new Document();
		
		if(post_id!=null) {
			q.put("post_id", post_id);
		}else if(comment_id!=null) {
			q.put("comment_id",comment_id);
		}
		
		q.put("login",login);
		
		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mc=db.getCollection("Likes");
		
		mc.insertOne(q);

		JSONObject resp=new JSONObject();
		try {
			resp.put("status", "ok");
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return resp;
	}
	
	
	public static JSONObject dislike(String login,String key,String post_id,String comment_id) {
		if(login==null||key==null||(post_id==null && comment_id==null)) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}

		Document q=new Document();
		
		if(post_id!=null) {
			q.put("post_id", post_id);
		}else if(comment_id!=null) {
			q.put("comment_id",comment_id);
		}
		
		q.put("login",login);

		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mc=db.getCollection("Likes");

		mc.deleteOne(q);

		JSONObject resp=new JSONObject();
		try {
			resp.put("status", "ok");
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return resp;
	}
}
