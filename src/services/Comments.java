package services;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import tools.UserTools;

public class Comments {

	public static JSONObject addComment(String login,String key,String post_id,String text) {
		if(login==null||key==null||post_id==null||text==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}

		int user_id=Integer.parseInt(tools.UserTools.getId(login));
		String comment_id="";
		LocalDateTime date = LocalDateTime.now();
		comment_id=Integer.toString(user_id)+date; 

		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mc=db.getCollection("Comments");

		Document d=new Document();
		d.append("user_id",user_id);
		d.append("login", login);
		d.append("post_id",post_id);
		d.append("comment_id",comment_id);
		d.append("date", date);
		d.append("text", text);
		mc.insertOne(d);
		JSONObject resp=new JSONObject();
		try {
			resp.put("status", "ok");
			resp.put("post_id",post_id);
			resp.put("comment_id",comment_id);
			resp.put("date", date);
		} catch (JSONException e1) {
			e1.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return resp;
	}

	public static JSONObject removeComment(String login,String key,String comment_id) {
		if(login==null||key==null||comment_id==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mcComments=db.getCollection("Comments");
		MongoCollection<Document> mcLikes=db.getCollection("Likes");
		Document d=new Document();
		d.append("comment_id",comment_id);
		mcLikes.deleteMany(d);
		try {
			mcComments.findOneAndDelete(d);
		}catch(NoSuchElementException e) {
			return services.Signal.serviceRefused(303);
		}
		JSONObject resp=new JSONObject();
		try {
			resp.put("status", "ok");
		} catch (JSONException e1) {
			e1.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return resp;
	}

	public static JSONObject editComment(String login,String key,String comment_id,String text) {
		if(login==null||key==null||comment_id==null||text==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mc=db.getCollection("Comments");

		Document q=new Document();
		q.put("comment_id", comment_id);		
		Document upd=new Document();
		upd.append("text", text);
		Document updOp=new Document();
		updOp.append("$set", upd);

		mc.updateOne(q, updOp);

		JSONObject resp=new JSONObject();
		try {
			resp.append("status", "ok");
			resp.append("message", "Votre commentaire à été modifié");
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return resp;
	}	
}
