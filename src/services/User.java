package services;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import tools.Database;
import tools.UserTools;

public class User {

	
	public static JSONObject createUser(String prenom, String nom, String login, String password, String sexe,String age){
		if(login==null||password==null||prenom==null||nom==null) {
			return Signal.serviceRefused(100);
		}
		if(tools.UserTools.userExists(login)) {
			return Signal.serviceRefused(201);			
		}		
		JSONObject res=new JSONObject();
		try {
			password=UserTools.hashPassword(password);
			Connection cn=Database.getMySQLConnection();
			String requete="Insert Into User (login,nom,prenom,password,sexe,age) values(\""+login+"\",\""+nom+"\",\""+prenom+"\",\""+password+"\",\""+sexe+"\","+age+");";
			Statement st= cn.createStatement();
			if(st.executeUpdate(requete)==1){
				st.close();
				cn.close();	
				try {
					res.put("status", "ok");
				} catch (JSONException e) {
					e.printStackTrace();
					return Signal.serviceRefused(401);
				}
				return res;//Signal.serviceAccepted("Inscription");
			}else {
				st.close();
				cn.close();		
				return Signal.serviceRefused(204);
			}
		}catch(SQLException e){
			e.printStackTrace();
			return Signal.serviceRefused(301);
		}
	}

	
	public static JSONObject login(String login, String password) {
		if(login==null||password==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login)) {
			return Signal.serviceRefused(202);			
		}	
		JSONObject resp=new JSONObject();
		try {
			password=UserTools.hashPassword(password);
			Connection cn=Database.getMySQLConnection();
			String requete="Select id,password from User where login='"+login+"';";
			Statement st= cn.createStatement();
			ResultSet rs=st.executeQuery(requete);
			rs.next();
			if (rs.getString("password").equals(password)) {
				int id=rs.getInt("id");
				rs.close();
				String key= UserTools.generateKey(id);
				Timestamp end_date=new Timestamp(System.currentTimeMillis());
				end_date=tools.UserTools.addMinutes(end_date,5);
					try {
						requete="Insert into Session (id,login,cle,end_date) values(\""+id+"\",\""+login+"\",\""+key+"\",\""+end_date+"\");";
						st.executeUpdate(requete);
						st.close();
						cn.close();
						try {
							resp.put("status", "ok");
							resp.put("key",key);
						} catch (JSONException e) {
							e.printStackTrace();
							return Signal.serviceRefused(401);
						}
						return resp;//Signal.serviceAccepted("Connexion établie!");	
					}catch (SQLException e) {
						key=tools.UserTools.getKey(login);
						requete="Update Session Set end_date=\""+end_date+"\" where id=\""+id+"\";";
						st.executeUpdate(requete);
						st.close();
						cn.close();
						try {
							resp.put("status", "ok");
							resp.put("key",key);
						} catch (JSONException e1) {
							e1.printStackTrace();
							return Signal.serviceRefused(401);
						}
						return resp;
					}
			} else {
				st.close();
				cn.close();
				return Signal.serviceRefused(203);			
			}
		} catch (SQLException e) {
			return Signal.serviceRefused(301);
		}
	}
	
	public static JSONObject logout(String login,String key) {
		if(login==null||key==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}
		try {
			Connection cn=Database.getMySQLConnection();
			String id=UserTools.getId(login);
			Statement st=cn.createStatement();
			
			String requete="Delete From Session Where id=\""+id+"\";";
			if(st.executeUpdate(requete)==1) {
				st.close();
				cn.close();
				JSONObject resp=new JSONObject();
				try {
					resp.put("status", "ok");
					resp.put("message", "Vous vous êtes déconnectés!");
				} catch (JSONException e1) {
					return Signal.serviceRefused(401);
				}
				return resp;
			}else {
				st.close();
				cn.close();
				return Signal.serviceRefused(207);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Signal.serviceRefused(301);
		}
	}
	
	
	public static JSONObject deleteAccount(String login, String key, String password) {
		if(login==null||key==null||password==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}
		String requete;
		try {
			Connection cn=Database.getMySQLConnection();
			Statement st=cn.createStatement();
			requete="Select password From User where login=\""+login+"\";";
			ResultSet res= st.executeQuery(requete);
			res.next();
			if (!res.getString("password").equals(tools.UserTools.hashPassword(password))) {
				return Signal.serviceRefused(203);
			}
			
			requete="Delete From User Where login=\""+login+"\";";
			if(st.executeUpdate(requete)==1) {
				st.close();
				cn.close();
				JSONObject resp=new JSONObject();
				try {
					resp.put("status", "ok");
					resp.put("message", "Votre compte à été supprimé!");
				} catch (JSONException e1) {
					return Signal.serviceRefused(401);
				}
				return resp;////Signal.serviceAccepted("L'utilisateur c'est déconnécté avec succés!");
			}else {
				st.close();
				cn.close();
				return Signal.serviceRefused(207);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Signal.serviceRefused(1);	
	}
	
	
	
	public static JSONObject getProfile(String login,String key,String login_profile) {
		if(login==null||key==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) || !tools.UserTools.userExists(login_profile)  ) {
			return Signal.serviceRefused(209);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}

		JSONObject res=new JSONObject();
		int user_id=Integer.parseInt(tools.UserTools.getId(login_profile));
		
		com.mongodb.client.MongoClient mongo=MongoClients.create();
		
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mongoCollectionPosts=db.getCollection("Posts");
		MongoCollection<Document> mongoCollectionLikes=db.getCollection("Likes");
		MongoCollection<Document> mongoCollectionComments=db.getCollection("Comments");

		Document questPosts=new Document();
		Document questLikes=new Document();
		Document questComments=new Document();
		Document questLikesComments=new Document();
		
		MongoCursor<Document>  cursorPosts;
		MongoCursor<Document>  cursorLikes;
		MongoCursor<Document>  cursorComments;
		MongoCursor<Document>  cursorLikesComments;
		
		JSONObject profile=new JSONObject();
		Connection cn;
		ArrayList<JSONObject> likeList;
		ArrayList<JSONObject> postList;
		ArrayList<JSONObject> comList;
		
		try {
			cn = Database.getMySQLConnection();
			Statement st=cn.createStatement();
			String requete="Select * from User where login=\""+login_profile+"\";";
			ResultSet result=st.executeQuery(requete);
			result.next();
			try {
				profile.put("nom", result.getString("nom"));
				profile.put("prenom", result.getString("prenom"));
				profile.put("login",result.getString("login"));
				profile.put("age", result.getString("age"));
				profile.put("sexe", result.getString("sexe"));
				profile.put("date",result.getString("date_sign_in"));
				res.put("profile", profile);//ici//
			} catch (JSONException e) {
				cn.close();
				st.close();
				e.printStackTrace();
				return services.Signal.serviceRefused(401);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
			services.Signal.serviceRefused(301);
		}
				
		JSONObject post=new JSONObject();
		JSONObject comment=new JSONObject();
		Document tmp;
		questPosts.append("user_id", user_id);
		cursorPosts=mongoCollectionPosts.find(questPosts).iterator();
		
		postList=new ArrayList<JSONObject>();
		while(cursorPosts.hasNext()) {
			try {
				tmp=cursorPosts.next();
				post=new JSONObject(tmp.toJson());
				String post_id=tmp.getString("post_id");
				
				questLikes=new Document();
				questLikes.append("post_id", post_id);
				cursorLikes=mongoCollectionLikes.find(questLikes).iterator();
			
				likeList=new ArrayList<JSONObject>();
				while(cursorLikes.hasNext()) {
					try {
						likeList.add(new JSONObject(cursorLikes.next().toJson()));
					} catch (JSONException e) {
						e.printStackTrace();
						return services.Signal.serviceRefused(401);
					}
				}
				post.put("likeList", likeList);
				
				questComments=new Document();
				questComments.append("post_id", post_id);
				questComments.append("login", login);
			
				cursorComments=mongoCollectionComments.find(questComments).iterator();				
				comList=new ArrayList<JSONObject>();
				while(cursorComments.hasNext()) {
					try {
						tmp=cursorComments.next();
						comment=new JSONObject(tmp.toJson());
						String comment_id=tmp.getString("comment_id");
						
						questLikesComments=new Document();
						questLikesComments.append("comment_id", comment_id);
						
						cursorLikesComments=mongoCollectionLikes.find(questLikesComments).iterator();
						likeList=new ArrayList<JSONObject>();
						while(cursorLikesComments.hasNext()) {
							try {
								likeList.add(new JSONObject(cursorLikesComments.next().toJson()));
							} catch (JSONException e) {
								e.printStackTrace();
								return services.Signal.serviceRefused(401);
							}
						}
						comment.put("likeList", likeList);
						comList.add(comment);
					} catch (JSONException e) {
						e.printStackTrace();
						return services.Signal.serviceRefused(401);
					}
				}
				post.put("comList",comList);
				postList.add(post);
			} catch (JSONException e) {
				e.printStackTrace();
				return Signal.serviceRefused(401);
			}
		}
		try {
			Collections.reverse(postList);
			res.put("postList",postList);
			res.put("status", "ok");
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		}

		return res;
	}

	public static JSONObject getHome(String login,String key) {
		if(login==null||key==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}

		JSONObject res=new JSONObject();
		int user_id=Integer.parseInt(tools.UserTools.getId(login));
		
		com.mongodb.client.MongoClient mongo=MongoClients.create();
		
		MongoDatabase db=mongo.getDatabase("3i017");
		MongoCollection<Document> mongoCollectionPosts=db.getCollection("Posts");
		MongoCollection<Document> mongoCollectionLikes=db.getCollection("Likes");
		MongoCollection<Document> mongoCollectionComments=db.getCollection("Comments");

		Document questPosts=new Document();
		Document questLikes=new Document();
		Document questComments=new Document();
		Document questLikesComments=new Document();
		
		MongoCursor<Document>  cursorPosts;
		MongoCursor<Document>  cursorLikes;
		MongoCursor<Document>  cursorComments;
		MongoCursor<Document>  cursorLikesComments;
		
		Document qbis=new Document();
		List<Integer> list=new ArrayList<Integer>();
		list.add(user_id);
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st=cn.createStatement();
			String requete="Select followed from Followers where follower=\""+user_id+"\";";
			ResultSet resbis=st.executeQuery(requete);
		
			while(resbis.next()) {
				list.add(resbis.getInt("followed"));
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(401);
		}
		
		qbis.put("$in", list);
		questPosts.put("user_id", qbis);
		cursorPosts=mongoCollectionPosts.find(questPosts).iterator();
		
		JSONObject post=new JSONObject();
		JSONObject comment=new JSONObject();
		ArrayList<JSONObject> likeList;
		ArrayList<JSONObject> postList;
		ArrayList<JSONObject> comList;
		Document tmp;
		postList=new ArrayList<JSONObject>();
		while(cursorPosts.hasNext()) {
			try {
				tmp = cursorPosts.next();
				post=new JSONObject(tmp.toJson());
				String post_id=tmp.getString("post_id");
				

				questLikes=new Document();
				questLikes.append("post_id", post_id);
				cursorLikes=mongoCollectionLikes.find(questLikes).iterator();
				likeList=new ArrayList<JSONObject>();
				while(cursorLikes.hasNext()) {
					try {
						likeList.add(new JSONObject(cursorLikes.next().toJson()));
					} catch (JSONException e) {
						e.printStackTrace();
						return services.Signal.serviceRefused(401);
					}
				}
				post.put("likeList", likeList);
				questComments=new Document();
				questComments.append("post_id", post_id);
				
				cursorComments=mongoCollectionComments.find(questComments).iterator();				
				comList=new ArrayList<JSONObject>();
				while(cursorComments.hasNext()) {
					try {
						tmp=cursorComments.next();
						comment=new JSONObject(tmp.toJson());
						String comment_id=tmp.getString("comment_id");
						questLikesComments=new Document();
						questLikesComments.append("comment_id", comment_id);
						
						cursorLikesComments=mongoCollectionLikes.find(questLikesComments).iterator();
						likeList=new ArrayList<JSONObject>();
						while(cursorLikesComments.hasNext()) {
							try {
								likeList.add(new JSONObject(cursorLikesComments.next().toJson()));
							} catch (JSONException e) {
								e.printStackTrace();
								return services.Signal.serviceRefused(401);
							}
						}
						comment.put("likeList", likeList);
						comList.add(comment);
					} catch (JSONException e) {
						e.printStackTrace();
						return services.Signal.serviceRefused(401);
					}
				}
				post.put("comList", comList);
				postList.add(post);
			} catch (JSONException e) {
				e.printStackTrace();
				return Signal.serviceRefused(401);
			}
		}
		try {
			Collections.reverse(postList);
			res.put("postList", postList);
			res.put("status", "ok");
		} catch (JSONException e) {
			e.printStackTrace();
			return Signal.serviceRefused(401);
		}
		return res;
	}
	
	
	
	public static JSONObject getAccountStats(String login,String key) {
		if(login==null||key==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}
		int nbfollowers=0;
		int nbfollowing=0;
		int nbposts=0;
		int nbcomments=0;
		int nblikes=0;
		String requete;
		ResultSet res;
		
		try {
			Connection cn=Database.getMySQLConnection();
			Statement st= cn.createStatement();
			
			requete="Select Count(*) From Followers Where follower=\""+tools.UserTools.getId(login)+"\";";
			res= st.executeQuery(requete);
			res.next();
			nbfollowing=res.getInt(1);
			
			requete="Select Count(*) From Followers Where followed=\""+tools.UserTools.getId(login)+"\";";
			res= st.executeQuery(requete);
			res.next();
			nbfollowers=res.getInt(1);
			
			st.close();
			cn.close();
		}catch(SQLException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(301);
		}
		
		com.mongodb.client.MongoClient mongo=MongoClients.create();
		MongoDatabase db=mongo.getDatabase("3i017");
		
		MongoCollection<Document> mc=db.getCollection("Posts");
		Document q=new Document();
		q.put("login", login);
		MongoCursor<Document> c=mc.find(q).iterator();
		while(c.hasNext()) {
			nbposts+=1;
			c.next();
		}
		
		mc=db.getCollection("Comments");
		q=new Document();
		q.put("login", login);
		c=mc.find(q).iterator();
		while(c.hasNext()) {
			nbcomments+=1;
			c.next();
		}
		
		mc=db.getCollection("Likes");
		q=new Document();
		q.put("login", login);
		c=mc.find(q).iterator();
		while(c.hasNext()) {
			nblikes+=1;
			c.next();
		}
		
		JSONObject resp=new JSONObject();
		try {
			resp.append("status", "ok");
			resp.append("nbfollowers", nbfollowers);
			resp.append("nbfollowing", nbfollowing);
			resp.append("nbposts", nbposts);
			resp.append("nbcomments", nbcomments);
			resp.append("nblikes", nblikes);
		} catch (JSONException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(401);
		}
		return resp;
	}
	
	
	public static JSONObject editProfile(String login,String key,String newN,String newP,String newAge,String newSexe) {
		if(login==null||key==null||newN==null||newP==null||newAge==null||newSexe==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}
	
		Connection cn;
		Statement st;
		String requete;
		JSONObject resp;
		try {
			cn = Database.getMySQLConnection();
			st=cn.createStatement();
			requete="Update User Set nom=\""+newN+"\", prenom=\""+newP+"\", age="+newAge+", sexe="+newSexe+" Where login=\""+login+"\";";
			if(st.executeUpdate(requete)!=1) {
				return services.Signal.serviceRefused(214);
			}
			try {
				resp=new JSONObject();
				resp.append("status", "ok");
				resp.append("message", "Votre profil a été modifié!");
			} catch (JSONException e) {
				e.printStackTrace();
				return services.Signal.serviceRefused(401);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(301);
		}
		return resp;
	}
	
	public static JSONObject newPassword(String login,String key,String newPW) {
		if(login==null||key==null) {
			return Signal.serviceRefused(100);	
		}
		if(!tools.UserTools.userExists(login) ) {
			return Signal.serviceRefused(202);			
		}
		if(!key.equals(tools.UserTools.getKey(login))) {
			services.User.logout(login,tools.UserTools.getKey(login));
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}
	
		Connection cn;
		Statement st;
		String requete;
		JSONObject resp;
		try {
			cn = Database.getMySQLConnection();
			st=cn.createStatement();
			requete="Update User Set password=\""+newPW+"\" Where login=\""+login+"\";";
			if(st.executeUpdate(requete)!=1) {
				return services.Signal.serviceRefused(214);
			}
			try {
				resp=new JSONObject();
				resp.append("status", "ok");
				resp.append("message", "Votre mot de passe a été modifié!");
			} catch (JSONException e) {
				e.printStackTrace();
				return services.Signal.serviceRefused(401);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return services.Signal.serviceRefused(301);
		}
		return resp;
	}
}
