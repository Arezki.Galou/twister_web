package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import tools.Database;
import tools.UserTools;

public class Followers {

	public static JSONObject follow(String login_follower,String key,String login_followed) {
		if(!tools.UserTools.userExists(login_follower) || !tools.UserTools.userExists(login_followed)) {
			return Signal.serviceRefused(209);			
		}  
		if (!UserTools.isLoggedIn(login_follower)) {
		 return Signal.serviceRefused(208);    
		}
		if(!key.equals(tools.UserTools.getKey(login_follower))) {
			services.User.logout(login_follower,key);
			return Signal.serviceRefused(305);
		}	
		if(login_follower.equals(login_followed)) {
			return Signal.serviceRefused(210);
		}

	
		try {
			Connection cn=Database.getMySQLConnection();
			Statement st= cn.createStatement();
			String id_follower=UserTools.getId(login_follower);
			String id_followed=UserTools.getId(login_followed);
			
			String requete="Select * From Followers Where follower=\""+id_follower+"\" And followed=\""+id_followed+"\";";
			ResultSet res= st.executeQuery(requete);
			if(res.next()) {
				st.close();
				cn.close();
				return Signal.serviceRefused(211);
			}
			requete="Insert Into Followers (follower,followed) values(\""+id_follower+"\",\""+id_followed+"\");";
			if(st.executeUpdate(requete)==1){
				st.close();
				cn.close();				
				JSONObject resp=new JSONObject();
				try {
					resp.put("status", "ok");
				} catch (JSONException e) {
					e.printStackTrace();
					return Signal.serviceRefused(401);
				}
				return resp;
			}else {
				st.close();
				cn.close();		
				return Signal.serviceRefused(212);
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		return Signal.serviceRefused(1);	
	}
	
	public static JSONObject removeFollower(String login_followed,String key,String login_follower) {
		if(login_follower==null||login_followed==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login_follower)||!tools.UserTools.userExists(login_followed)) {
			return Signal.serviceRefused(209);
		}
		if(!key.equals(tools.UserTools.getKey(login_followed))) {
			services.User.logout(login_followed,key);
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login_followed)) {
			 return Signal.serviceRefused(208);    
		}
			
		String id_follower=tools.UserTools.getId(login_follower);
		String id_followed=tools.UserTools.getId(login_followed);
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st=cn.createStatement();
			String requete="Delete from Followers where follower=\""+id_follower+"\" And followed=\""+id_followed+"\";";
			if(st.executeUpdate(requete)==1) {
				st.close();
				cn.close();		
				JSONObject resp=new JSONObject();
				try {
					resp.put("status", "ok");
				} catch (JSONException e) {
					e.printStackTrace();
					return Signal.serviceRefused(401);
				}
				return resp;
			}else {
				st.close();
				cn.close();				
				return Signal.serviceRefused(213);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Signal.serviceRefused(1);
	}
	
	public static JSONObject unfollow(String login_follower,String key,String login_followed) {
		if(login_follower==null||key==null||login_followed==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login_follower)||!tools.UserTools.userExists(login_followed)) {
			return Signal.serviceRefused(209);
		}
		if(!key.equals(tools.UserTools.getKey(login_follower))) {
			services.User.logout(login_follower,key);
			return Signal.serviceRefused(305);
		}
		if (!UserTools.isLoggedIn(login_follower)) {
			 return Signal.serviceRefused(208);    
		}
			
		String id_follower=tools.UserTools.getId(login_follower);
		String id_followed=tools.UserTools.getId(login_followed);
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st=cn.createStatement();
			String requete="Delete from Followers where follower=\""+id_follower+"\" And followed=\""+id_followed+"\";";
			if(st.executeUpdate(requete)==1) {
				st.close();
				cn.close();		
				JSONObject resp=new JSONObject();
				try {
					resp.put("status", "ok");
				} catch (JSONException e) {
					e.printStackTrace();
					return Signal.serviceRefused(401);
				}
				return resp;
			}else {
				st.close();
				cn.close();				
				return Signal.serviceRefused(213);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Signal.serviceRefused(1);
	}
	
	public static JSONObject getUserFollowers(String login, String key) {
		if(login==null||key==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login)) {
			return Signal.serviceRefused(202);
		}
		if (!UserTools.isLoggedIn(login)) {
			 return Signal.serviceRefused(208);    
		}
			
		JSONObject resp=new JSONObject();
		int id=Integer.parseInt(tools.UserTools.getId(login));
		List<Integer> followers=new ArrayList<Integer>();
		List<Integer> following=new ArrayList<Integer>();
		List<JSONObject> followersList=new ArrayList<JSONObject>();
		int val;		
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st=cn.createStatement();
			
			String requete="Select * from Followers";
			ResultSet res=st.executeQuery(requete);
			while(res.next()) {
				if(id==res.getInt("followed")) {
					followers.add(res.getInt("follower"));
				}else if(id==res.getInt("follower")){
					following.add(res.getInt("followed"));
					
				}
			}
			JSONObject obj;
			for(int f:followers) {
				obj=new JSONObject();
				requete="Select * from User where id=\""+f+"\";";///ici///
				ResultSet resbis=st.executeQuery(requete);
				resbis.next();
				try {
						val=resbis.getInt("id");
						obj.put("login",resbis.getString("login"));
						obj.put("nom", resbis.getString("nom"));
						obj.put("prenom", resbis.getString("prenom"));
						obj.put("age", resbis.getString("age"));
						obj.put("sexe", resbis.getString("sexe"));
						if(following.contains(val)) {
							obj.put("type", "frfd");
							followersList.add(obj);
						}else{
							obj.put("type", "fr");
							followersList.add(obj);
						}
					
				} catch (JSONException e) {
					st.close();
					cn.close();	
					return services.Signal.serviceRefused(401);
				}
			}
		
			try {
				resp.put("followersList", followersList);
				resp.put("status", "ok");
			} catch (JSONException e) {
				st.close();
				cn.close();	
				return Signal.serviceRefused(401);
			}
			st.close();
			cn.close();	
			return resp;
		}catch(SQLException e){
			e.printStackTrace();
			return Signal.serviceRefused(301);
		}
	}
	
	
	public static JSONObject getUserFollowing(String login, String key) {
		if(login==null||key==null) {
			return Signal.serviceRefused(100);
		}
		if(!tools.UserTools.userExists(login)) {
			return Signal.serviceRefused(202);
		}
		if (!UserTools.isLoggedIn(login)) {
			return Signal.serviceRefused(208);    
		}
				
		int id=Integer.parseInt(tools.UserTools.getId(login));
		List<Integer> followers=new ArrayList<Integer>();
		List<Integer> following=new ArrayList<Integer>();
		List<JSONObject> followingList=new ArrayList<JSONObject>();
		int val;
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st=cn.createStatement();
			
			String requete="Select * from Followers";
			ResultSet res=st.executeQuery(requete);
			while(res.next()) {
				if(id==res.getInt("followed")) {
					followers.add(res.getInt("follower"));
				}else if(id==res.getInt("follower")){
					following.add(res.getInt("followed"));
					
				}
			}

			JSONObject resp=new JSONObject();
			JSONObject obj;
			for(int f: following) {
				obj=new JSONObject();
				System.out.println(f);
				requete="Select * from User where id=\""+f+"\";";
				ResultSet resbis=st.executeQuery(requete);
				resbis.next();
				try {
						val=resbis.getInt("id");
						obj.put("login", resbis.getString("login"));
						obj.put("nom", resbis.getString("nom"));
						obj.put("prenom", resbis.getString("prenom"));
						obj.put("age", resbis.getString("age"));
						obj.put("sexe", resbis.getString("sexe"));
						if(followers.contains(val)) {
							obj.put("type", "frfd");
							followingList.add(obj);
						}else{
							obj.put("type", "fd");
							followingList.add(obj);
						}
				} catch (JSONException e) {
					st.close();
					cn.close();
					e.printStackTrace();
					return services.Signal.serviceRefused(401);
				}
			}

			try {
				resp.put("followingList", followingList);
				resp.put("status", "ok");
			} catch (JSONException e) {
				e.printStackTrace();
				return Signal.serviceRefused(401);
			}
			st.close();
			cn.close();	
			return resp;
		}catch(SQLException e){
			e.printStackTrace();
			return Signal.serviceRefused(301);
		}
	}
}
