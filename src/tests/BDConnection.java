package tests;

import java.sql.Connection;

import java.sql.*;
import tools.Database;

public class BDConnection {
	
	public static void main(String[] args) {
		try {
			Connection cn=Database.getMySQLConnection();
			String requete="Select * From User;";
			Statement st= cn.createStatement();
			ResultSet res=st.executeQuery(requete);
			while(res.next()) {
				System.out.println(res.getString("nom")+"\t"+res.getInt("id"));
			}
			st.close();
			cn.close();
		}catch(Exception e){
			e.getMessage();
		}
	}
}
