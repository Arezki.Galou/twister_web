package tools;

import java.sql.*;
import javax.naming.*;
import javax.sql.*;

public class Database {
	private DataSource dataSource;
	public static Database database=null;
	
	public Database(String jndiname) throws SQLException{
		try {
			dataSource =(DataSource) new InitialContext().lookup("java:comp/env/"+jndiname);
			
		} catch (NamingException e) {
			// TODO: handle exception
			throw new SQLException(jndiname+"is missing in JNDI :"+e.getMessage());
		}
	}
	
	public Connection getConnection()throws SQLException{
		return dataSource.getConnection();
	}
	
	public static Connection getMySQLConnection()throws SQLException{
		try {
			Class.forName("com.mysql.jdbc.Driver"); 
		} catch(java.lang.ClassNotFoundException e) {
			 System.err.print("Exception: ");
			 System.err.println(e.getMessage());
		}
		
		if (DBStatic.mysql_pooling==false) {
			return (DriverManager.getConnection("jdbc:mysql://"+DBStatic.mysql_host+"/"+DBStatic.mysql_db,DBStatic.mysql_username,DBStatic.mysql_password));
		} else {
			if (database==null) {
				database=new Database("jdbc/hichem_kiki");
			} 
			return (database.getConnection());
		}
	}
}
