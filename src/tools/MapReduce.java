package tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.client.MapReduceIterable;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;


public class MapReduce {
	static String mapDF="function map(){" +
			"var text=this.text;" +
			"var words=text.match(/\\w+/g);" +
			"var df=[];" +
			"for (w in words){" +
				"if(df[words[w]]==null){" +
					"df[words[w]]=1;" +
				"}for(w in df){" +
					"emit(w,{df:1});" +
				"}" +
			"}" +
		"}";

	static String reduceDF="function reduce(key, values){	" +
			"var total=0;	" +
			"for(i in values){		" +
				"total+=values[i].df;	" +
				"};	" +
				"return({word: key, df:total});" +
				"}";
	

	static String mapTF="function mapTF(){	" +
			"						var text=this.text;	" +
									"var words=text.match(/\\w+/g);	" +
									"var tf=[];	" +
									"for(i in words){" +
										"if(tf[words[i]]==null){	" +
											"tf[words[i]]=1;" +
											"}else{" +
												"tf[words[i]]++;" +
											"}" +
									"}" +
									"for(w in tf){" +
									"	emit(this._id, {words: w, tf: tf[w]});" +
									"}" +
								"}";
	
	static String reduceTF="function reduceTF(key, value){	" +
			"return({doc: key, tfs: value});" +
			"}";
	
	private static void indexDf(Connection conn, MongoCollection<Document> collection) throws SQLException{
		MapReduceIterable<Document> outDF=collection.mapReduce(mapDF,reduceDF);
		for(Document obj: outDF){
			Document value= (Document) obj.get("value");	
			String word=(String) obj.get("_id");
			
			int df=((Double)value.get("df")).intValue();

			Statement stDf=conn.createStatement();
			
			String queryDf;
			queryDf="Update t_df Set df=df+"+df+" Where word=\""+word+"\";";
			if(stDf.executeUpdate(queryDf)==0) {
			queryDf="Insert Into t_df(word, df) Values (\""+word+"\","+df+");";
			stDf.executeUpdate(queryDf);
			}
			stDf.close();			
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private static void indexTf(Connection conn, MongoCollection<Document> collection) throws SQLException{
		MapReduceIterable<Document> outDF=collection.mapReduce(mapTF,reduceTF);
		for (Document document : outDF) {
			Document value = (Document) document.get("value");
			String post_id=((document.get("_id"))).toString();
			ArrayList<Object> tfs=new ArrayList<Object>();
			if(value.containsKey("tfs"))
				tfs= (ArrayList<Object>) value.get("tfs");
			else{
				
				tfs.add(value);
			}
			for (Object ob2 : tfs) {
				Document inst= (Document) ob2;
				String words=(String)inst.get("words");
				int tf=(int)((Double)inst.get("tf")).intValue();
				Statement stTf=conn.createStatement();
				String queryTf;
				queryTf="Update t_tf Set tf=tf+"+tf+" Where document=\""+document+"\" And word=\""+words+"\";"; // INTO t_tf(document, word, tf) VALUES ('"+ post_id +"','"+words+"',"+tf+")";
				if(stTf.executeUpdate(queryTf)==0) {
					queryTf="Insert Into t_tf(document, word, tf) Values ('"+ post_id +"','"+words+"',"+tf+")";
					stTf.executeUpdate(queryTf);
				}
				stTf.close();
			}
		}
	}
	public static void reinitTables(Connection conn) throws SQLException{
		String queryDelete= "Delete From t_df Where 1;";
		Statement stDeleteDf=(Statement) conn.createStatement();
		stDeleteDf.execute(queryDelete);
		stDeleteDf.close();
		queryDelete= "Delete From t_tf Where 1;";
		Statement stDeleteTf=(Statement) conn.createStatement();
		stDeleteTf.execute(queryDelete);
		stDeleteTf.close();
	}
	
	

public static JSONObject recherche(String query) throws SQLException, JSONException{
	reinitTables(Database.getMySQLConnection());
	com.mongodb.client.MongoClient mongo=MongoClients.create();
	MongoDatabase db=mongo.getDatabase("3i017");
	MongoCollection<Document> mcPosts=db.getCollection("Posts");
	MongoCollection<Document> mcComments=db.getCollection("Comments");
	
	
	
	indexDf(Database.getMySQLConnection(),mcPosts );
	indexTf(Database.getMySQLConnection(),mcPosts );
	indexDf(Database.getMySQLConnection(),mcComments );
	indexTf(Database.getMySQLConnection(),mcComments );
	
	
	JSONObject res=new JSONObject();
	 
	Pattern pattern = Pattern.compile ("\\w+");
	Matcher matcher = pattern.matcher(query);
	ArrayList<String> words=new ArrayList<String>();
	while(matcher.find())
		words.add(matcher.group());
	StringBuffer listWords=new StringBuffer(256);
	listWords.append('(');
	listWords.append("'"+words.get(0)+"'");
	for(int i=1; i<words.size(); i++){
		listWords.append(",'"+words.get(i)+"'");
	}
	listWords.append(')');
	String requete="Select document as post_id, sum(tf_idf) As rsv " +
					"From (SELECT document, log(tf*log(8/df)) As tf_idf " +
							"From t_tf, t_df " +
							"Where t_df.word in "+listWords+" And t_df.word=t_tf.word) "
					+ "As tab_idf Group By document Order By rsv";
	
	ArrayList<String> postIdList=new ArrayList<String>();
	Connection conn = Database.getMySQLConnection();
	Statement st=conn.createStatement();
	
	ResultSet rs=st.executeQuery(requete);
	while(rs.next()){
		postIdList.add(rs.getString("post_id"));
	}
	System.out.println(postIdList);
	res.put("postList",postIdList);
	rs.close();
	st.close();
	conn.close();
	return res;
}
}













