package tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class UserTools {
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	public static boolean userExists(String login) {
		try {
			Connection cn=Database.getMySQLConnection();
			String requete="Select login From User;";
			Statement st= cn.createStatement();
			ResultSet res=st.executeQuery(requete);
			while(res.next()) {
				if(res.getString("login").equals(login)) {
					st.close();
					cn.close();					
					return true;
				}
			}
			st.close();
			cn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public static String generateKey(int identifiant) {
		String id= Integer.toString(identifiant);
		while(id.length()<7) {
			id="0"+id;
		}
		int count=id.length();
		int i;
		String key=id;
		while(count<32) {
			i=(int)(Math.random()*ALPHA_NUMERIC_STRING.length());
			key+=ALPHA_NUMERIC_STRING.charAt(i);
			count++;
		}
		return key;
	}

	public static String getId(String login) {
		if(!tools.UserTools.userExists(login)){
			return null;
		}
		try {
			Connection cn=Database.getMySQLConnection();
			String requete="Select id from User where login=\""+login+"\";";
			Statement st= cn.createStatement();
			ResultSet res=st.executeQuery(requete);
			res.next();
			String id= res.getString("id");
			st.close();
			cn.close();
			return id;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getLogin(String nom, String prenom) {
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st= cn.createStatement();
			String requete="Select login From User Where nom=\""+nom+"\" And prenom=\""+prenom+"\";";
			ResultSet res=st.executeQuery(requete);
			if(!res.next()) {
				st.close();
				cn.close();			
				return null;
			}
			String login= res.getString("login");
			st.close();
			cn.close();
			return login;
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static String getLoginById(String id) {
		try {
			Connection cn = Database.getMySQLConnection();
			Statement st= cn.createStatement();
			String requete="Select login From User Where id=\""+id+"\";";
			ResultSet res=st.executeQuery(requete);
			if(!res.next()) {
				st.close();
				cn.close();			
				return null;
			}
			String login= res.getString("login");
			st.close();
			cn.close();
			return login;
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static String hashPassword(String password) {
		String hash;
		StringBuilder sbhash=new StringBuilder();
		int val;
		for(int i=0;i<password.length();i++) {
			val=password.charAt(i);
			sbhash.append(String.valueOf(val*3));
		}
		hash=sbhash.toString();
		return hash;
	}
	

	public static Timestamp addMinutes(Timestamp timestamp, int nbMinutes){
		Calendar cal = Calendar.getInstance();
	    cal.setTimeInMillis(timestamp.getTime());
	    cal.add(Calendar.MINUTE, nbMinutes);
	    timestamp = new Timestamp(cal.getTime().getTime());
	    return timestamp;
	}
	

	public static boolean isLoggedIn(String login) {
		String id=getId(login);
		boolean ret=true;
		try {
			Connection cn=Database.getMySQLConnection();
			Statement st= cn.createStatement();
			String requete="Select end_date From Session where id=\""+id+"\";";
			ResultSet rs=st.executeQuery(requete);
			
			if(rs.next()) {
				Timestamp timestamp = new Timestamp(new Date().getTime());
				if (timestamp.after(rs.getTimestamp("end_date"))) {
					services.User.logout(login,tools.UserTools.getKey(login));
	                ret=false;
					st.close();
					cn.close();	
				}else{
					Timestamp end_date=new Timestamp(System.currentTimeMillis());
					end_date=tools.UserTools.addMinutes(end_date,30);
					requete="Update Session Set end_date=\""+end_date+"\" where id=\""+id+"\";";
					st.executeUpdate(requete);
					st.close();
					cn.close();
				}
			}else{
				st.close();
				cn.close();
				ret=false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public static String getKey(String login) {
		try {
			Connection cn=Database.getMySQLConnection();
			Statement st= cn.createStatement();
			String id= getId(login);
			String requete="Select * From Session where id=\""+id+"\";";
			ResultSet rs=st.executeQuery(requete);
			if(rs.next()) {
				String key=rs.getString("cle");
				st.close();
				cn.close();				
				return key;
			}
			st.close();
			cn.close();				
			return null;
		} catch (SQLException e) {		
			e.printStackTrace();
		}
		return null;
	}
}



