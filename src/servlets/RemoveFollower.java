package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;


public class RemoveFollower extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login_followed=request.getParameter("login");
		String key=request.getParameter("key");
		String login_follower=request.getParameter("login_follower");
		PrintWriter out=response.getWriter();
		JSONObject res=services.Followers.removeFollower(login_followed,key, login_follower);
		out.println(res);
	}
}
