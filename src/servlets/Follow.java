package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;


public class Follow extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login_follower=request.getParameter("login");
		String key=request.getParameter("key");
		String login_followed=request.getParameter("login_followed");
		PrintWriter out=response.getWriter();
		JSONObject res=services.Followers.follow(login_follower,key,login_followed);
		out.println(res);
	}
}
