package servlets;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;


public class EditProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login=request.getParameter("login");
		String key=request.getParameter("key");
		String newN=request.getParameter("new_nom");
		String newP=request.getParameter("new_prenom");
		String newAge=request.getParameter("new_age");
		String newSexe=request.getParameter("new_sexe");
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		JSONObject res=services.User.editProfile(login, key, newN, newP, newAge, newSexe);
		out.println(res);
	}
}