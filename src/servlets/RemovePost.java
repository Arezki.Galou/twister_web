package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class RemovePost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		String login=request.getParameter("login");
		String post_id=request.getParameter("post_id");
		String key=request.getParameter("key");
		response.setContentType("text/plain");
		PrintWriter out;
		out = response.getWriter();
		JSONObject res;
		res=services.Posts.removePost(login,key, post_id);
		out.println(res);
	
	
	}

}
