package servlets;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;


public class AddPost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login=request.getParameter("login");
		String text=request.getParameter("text");
		String key=request.getParameter("key");
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		JSONObject res=services.Posts.addPost(login,key,text);
		out.println(res);
	}
}
