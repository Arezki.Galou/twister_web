package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;


public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String key=request.getParameter("key");
		String login=request.getParameter("login");
		String login_profile=request.getParameter("login_profile");
		PrintWriter out=response.getWriter();
		JSONObject res=services.User.getProfile(login,key,login_profile);
		out.println(res);
	}
}
