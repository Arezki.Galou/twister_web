package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class DeleteAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String login=request.getParameter("login");
		String key=request.getParameter("key");
		String password=request.getParameter("password");
		response.setContentType("text/plain");
		PrintWriter out;
		out = response.getWriter();
		JSONObject res;
		res = services.User.deleteAccount(login, key, password);
		out.println(res);
	}
}
