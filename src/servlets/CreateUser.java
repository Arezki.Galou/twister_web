package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String prenom=request.getParameter("prenom");
		String nom=request.getParameter("nom");			
		String login=request.getParameter("login");
		String password=request.getParameter("password");
		String sexe=request.getParameter("sexe");
		String age=request.getParameter("age");
		response.setContentType("text/plain");
		PrintWriter out;
		out = response.getWriter();
		JSONObject res;
		res = services.User.createUser(prenom,nom,login,password,sexe,age);
		out.println(res);
	}
}
