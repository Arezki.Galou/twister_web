package servlets;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;


public class EditComment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login=request.getParameter("login");
		String text=request.getParameter("text");
		String key=request.getParameter("key");
		String comment_id=request.getParameter("comment_id");
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		JSONObject res=services.Comments.editComment(login,key,comment_id, text);
		out.println(res);
	}
}